//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2017.12.21 at 05:58:11 PM GMT+07:00 
//


package extract.kpt;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Адрес (описание местоположения) до уровня населённого пункта
 * 
 * <p>Java class for AddressCity complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="AddressCity">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="fias" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="okato" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="kladr" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="oktmo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="postal_code" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;length value="6"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="region" type="{}Dict"/>
 *         &lt;element name="district" type="{}District" minOccurs="0"/>
 *         &lt;element name="city" type="{}City" minOccurs="0"/>
 *         &lt;element name="urban_district" type="{}UrbanDistrict" minOccurs="0"/>
 *         &lt;element name="soviet_village" type="{}SovietVillage" minOccurs="0"/>
 *         &lt;element name="locality" type="{}Locality" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AddressCity", propOrder = {
    "fias",
    "okato",
    "kladr",
    "oktmo",
    "postalCode",
    "region",
    "district",
    "city",
    "urbanDistrict",
    "sovietVillage",
    "locality"
})
public class AddressCity {

    protected String fias;
    protected String okato;
    protected String kladr;
    protected String oktmo;
    @XmlElement(name = "postal_code")
    protected String postalCode;
    @XmlElement(required = true)
    protected Dict region;
    protected District district;
    protected City city;
    @XmlElement(name = "urban_district")
    protected UrbanDistrict urbanDistrict;
    @XmlElement(name = "soviet_village")
    protected SovietVillage sovietVillage;
    protected Locality locality;

    /**
     * Gets the value of the fias property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFias() {
        return fias;
    }

    /**
     * Sets the value of the fias property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFias(String value) {
        this.fias = value;
    }

    /**
     * Gets the value of the okato property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOkato() {
        return okato;
    }

    /**
     * Sets the value of the okato property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOkato(String value) {
        this.okato = value;
    }

    /**
     * Gets the value of the kladr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getKladr() {
        return kladr;
    }

    /**
     * Sets the value of the kladr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setKladr(String value) {
        this.kladr = value;
    }

    /**
     * Gets the value of the oktmo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOktmo() {
        return oktmo;
    }

    /**
     * Sets the value of the oktmo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOktmo(String value) {
        this.oktmo = value;
    }

    /**
     * Gets the value of the postalCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPostalCode() {
        return postalCode;
    }

    /**
     * Sets the value of the postalCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPostalCode(String value) {
        this.postalCode = value;
    }

    /**
     * Gets the value of the region property.
     * 
     * @return
     *     possible object is
     *     {@link Dict }
     *     
     */
    public Dict getRegion() {
        return region;
    }

    /**
     * Sets the value of the region property.
     * 
     * @param value
     *     allowed object is
     *     {@link Dict }
     *     
     */
    public void setRegion(Dict value) {
        this.region = value;
    }

    /**
     * Gets the value of the district property.
     * 
     * @return
     *     possible object is
     *     {@link District }
     *     
     */
    public District getDistrict() {
        return district;
    }

    /**
     * Sets the value of the district property.
     * 
     * @param value
     *     allowed object is
     *     {@link District }
     *     
     */
    public void setDistrict(District value) {
        this.district = value;
    }

    /**
     * Gets the value of the city property.
     * 
     * @return
     *     possible object is
     *     {@link City }
     *     
     */
    public City getCity() {
        return city;
    }

    /**
     * Sets the value of the city property.
     * 
     * @param value
     *     allowed object is
     *     {@link City }
     *     
     */
    public void setCity(City value) {
        this.city = value;
    }

    /**
     * Gets the value of the urbanDistrict property.
     * 
     * @return
     *     possible object is
     *     {@link UrbanDistrict }
     *     
     */
    public UrbanDistrict getUrbanDistrict() {
        return urbanDistrict;
    }

    /**
     * Sets the value of the urbanDistrict property.
     * 
     * @param value
     *     allowed object is
     *     {@link UrbanDistrict }
     *     
     */
    public void setUrbanDistrict(UrbanDistrict value) {
        this.urbanDistrict = value;
    }

    /**
     * Gets the value of the sovietVillage property.
     * 
     * @return
     *     possible object is
     *     {@link SovietVillage }
     *     
     */
    public SovietVillage getSovietVillage() {
        return sovietVillage;
    }

    /**
     * Sets the value of the sovietVillage property.
     * 
     * @param value
     *     allowed object is
     *     {@link SovietVillage }
     *     
     */
    public void setSovietVillage(SovietVillage value) {
        this.sovietVillage = value;
    }

    /**
     * Gets the value of the locality property.
     * 
     * @return
     *     possible object is
     *     {@link Locality }
     *     
     */
    public Locality getLocality() {
        return locality;
    }

    /**
     * Sets the value of the locality property.
     * 
     * @param value
     *     allowed object is
     *     {@link Locality }
     *     
     */
    public void setLocality(Locality value) {
        this.locality = value;
    }

}
