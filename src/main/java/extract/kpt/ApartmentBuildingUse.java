//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2017.12.21 at 05:58:11 PM GMT+07:00 
//


package extract.kpt;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Сведения о создании (эксплуатации) на земельном участке наемного дома (в т.ч. вид использвания и назначение ЗУ)
 * 
 * <p>Java class for ApartmentBuildingUse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ApartmentBuildingUse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="purpose_hired_home" type="{}Dict" minOccurs="0"/>
 *         &lt;element name="use_apart_build" type="{}Dict" minOccurs="0"/>
 *         &lt;element name="basis_hired_house" type="{}BasisHiredHouse" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ApartmentBuildingUse", propOrder = {
    "purposeHiredHome",
    "useApartBuild",
    "basisHiredHouse"
})
public class ApartmentBuildingUse {

    @XmlElement(name = "purpose_hired_home")
    protected Dict purposeHiredHome;
    @XmlElement(name = "use_apart_build")
    protected Dict useApartBuild;
    @XmlElement(name = "basis_hired_house")
    protected BasisHiredHouse basisHiredHouse;

    /**
     * Gets the value of the purposeHiredHome property.
     * 
     * @return
     *     possible object is
     *     {@link Dict }
     *     
     */
    public Dict getPurposeHiredHome() {
        return purposeHiredHome;
    }

    /**
     * Sets the value of the purposeHiredHome property.
     * 
     * @param value
     *     allowed object is
     *     {@link Dict }
     *     
     */
    public void setPurposeHiredHome(Dict value) {
        this.purposeHiredHome = value;
    }

    /**
     * Gets the value of the useApartBuild property.
     * 
     * @return
     *     possible object is
     *     {@link Dict }
     *     
     */
    public Dict getUseApartBuild() {
        return useApartBuild;
    }

    /**
     * Sets the value of the useApartBuild property.
     * 
     * @param value
     *     allowed object is
     *     {@link Dict }
     *     
     */
    public void setUseApartBuild(Dict value) {
        this.useApartBuild = value;
    }

    /**
     * Gets the value of the basisHiredHouse property.
     * 
     * @return
     *     possible object is
     *     {@link BasisHiredHouse }
     *     
     */
    public BasisHiredHouse getBasisHiredHouse() {
        return basisHiredHouse;
    }

    /**
     * Sets the value of the basisHiredHouse property.
     * 
     * @param value
     *     allowed object is
     *     {@link BasisHiredHouse }
     *     
     */
    public void setBasisHiredHouse(BasisHiredHouse value) {
        this.basisHiredHouse = value;
    }

}
