//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2017.12.21 at 05:58:11 PM GMT+07:00 
//


package extract.kpt;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Сведения об объекте недвижимости (вид, кадастровый номер, номер кадастрового квартала)
 * 
 * <p>Java class for CommonData complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CommonData">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="cad_number" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="quarter_cad_number" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="type" type="{}Dict"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CommonData", propOrder = {
    "cadNumber",
    "quarterCadNumber",
    "type"
})
public class CommonData {

    @XmlElement(name = "cad_number", required = true)
    protected String cadNumber;
    @XmlElement(name = "quarter_cad_number", required = true)
    protected String quarterCadNumber;
    @XmlElement(required = true)
    protected Dict type;

    /**
     * Gets the value of the cadNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCadNumber() {
        return cadNumber;
    }

    /**
     * Sets the value of the cadNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCadNumber(String value) {
        this.cadNumber = value;
    }

    /**
     * Gets the value of the quarterCadNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getQuarterCadNumber() {
        return quarterCadNumber;
    }

    /**
     * Sets the value of the quarterCadNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setQuarterCadNumber(String value) {
        this.quarterCadNumber = value;
    }

    /**
     * Gets the value of the type property.
     * 
     * @return
     *     possible object is
     *     {@link Dict }
     *     
     */
    public Dict getType() {
        return type;
    }

    /**
     * Sets the value of the type property.
     * 
     * @param value
     *     allowed object is
     *     {@link Dict }
     *     
     */
    public void setType(Dict value) {
        this.type = value;
    }

}
