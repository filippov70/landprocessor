/*
 * LandProcessor - конвертер XML Росреестра в Shape-файлы
 * Copyright 2011 Филиппов Владислав, aka nukevlad, aka filippov70
 * filippov70@gmail.com Россия, Томск
 * 
 * Этот файл — часть LandProcessor.

   LandProcessor - свободная программа: вы можете перераспространять её и/или
   изменять её на условиях Стандартной общественной лицензии GNU в том виде,
   в каком она была опубликована Фондом свободного программного обеспечения;
   либо версии 3 лицензии, либо (по вашему выбору) любой более поздней
   версии.

   LandProcessor распространяется в надежде, что она будет полезной,
   но БЕЗО ВСЯКИХ ГАРАНТИЙ; даже без неявной гарантии ТОВАРНОГО ВИДА
   или ПРИГОДНОСТИ ДЛЯ ОПРЕДЕЛЕННЫХ ЦЕЛЕЙ. Подробнее см. в Стандартной
   общественной лицензии GNU.

   Вы должны были получить копию Стандартной общественной лицензии GNU
   вместе с этой программой. Если это не так, см.
   <http://www.gnu.org/licenses/>.)
 */

package org.tomskgislab.landprocessor;
import org.opengis.referencing.FactoryException;
import org.opengis.referencing.crs.CRSAuthorityFactory;
import org.opengis.referencing.crs.CoordinateReferenceSystem;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.util.*;
import org.geotools.referencing.CRS;
import org.opengis.referencing.operation.MathTransform;

/**
 * Copyright Denis Pershin 2013, Filippov 2014
 * CLI
 * Класс предназначен для массовой обработки всех XML в одной директории
 */
public class CmdLine {
    private static List<LandProcessor> processors = new ArrayList<>();

    // http://skipy.ru/technics/encodings_console_comp.html
    public static void main(String[] args) {
        String consoleEncoding = System.getProperty("consoleEncoding");
        System.setProperty("org.geotools.referencing.forceXY", "true");
        if (consoleEncoding != null) {
            try {
                System.setOut(new PrintStream(System.out, true, consoleEncoding));
            } catch (java.io.UnsupportedEncodingException ex) {
                System.err.println("Unsupported encoding set for console: " + consoleEncoding);
            }
        }
        String format = "shp";
        Boolean isMerged = false;
        Boolean createPoint = false;
        Boolean toWGS = false;
        if (args.length < 1) {
            usage();
            return;
        } else {
            for (String arg : args){
                switch (arg) {
                    case "-shp":
                        format = "shp";
                        break;
                    case "-json":
                        format = "geojson";
                        break;
                    case "-merge":
                        isMerged = true;
                        break;
                    case "-wgs":
                        toWGS = true;
                        // longitude first
                        CRSAuthorityFactory factory = CRS.getAuthorityFactory(true);
                        break;
                    case "-points":
                        createPoint = true;
                        break;
                }
            }
        }
        String savePath = args[args.length - 1];// + "/convert";

        //savePath = args[2] + File.separator + "merged_";//savePath = args[2] + args[3].substring(args[3].lastIndexOf(" "));

        File[] fList;
        File checkDirectory = new File(savePath + "/convert");
        if (!checkDirectory.exists()) {
            checkDirectory.mkdir();
        }
        File F = new File(savePath);
        fList = F.listFiles();
        if (fList != null) {
            for (int i = 0; i < fList.length; i++) {
                try {
                    String extension = "";

                    int e = fList[i].getName().lastIndexOf('.');
                    if (e > 0) {
                        extension = fList[i].getName().substring(e + 1);
                    }
                    if (fList[i].isFile() && extension.equals("xml")) {
                        LandProcessor processor;
                        processor = new LandProcessor();
                        processor.ProcessXMLFile(fList[i], isMerged, createPoint);

                        if (!isMerged) {
                            String outputFileName = e > 0
                                    ? fList[i].getName().substring(0, e)
                                    : fList[i].getName() + "-out";
                            File outputFile = new File(savePath + "/convert", outputFileName);
                            save(outputFile, format, processor);
                        } else if (!processor.isEmpty()) {
                            processors.add(processor);
                        } else {
                            System.out.println("Warning, quartal is empty! " + fList[i].getName());
                        }
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
            if (isMerged && !toWGS) {
                sortProcessorsAndSave(processors, savePath, format);
                //save(new File(savePath + "/convert", "convertMergedResult"), format);
            } else if (isMerged && toWGS) {
                try {
                    toWGSAndSave(processors, savePath, format);
                } catch (FactoryException e) {
                    e.printStackTrace();
                }
            }
        } else {
            System.out.println("Исходная директория пуста.");
        }

    }

    // только для МСК-50, работает через ключ -wgs
    // не использовать
    private static void toWGSAndSave (List<LandProcessor> processors, String savePath, String format) throws FactoryException {
        CoordinateReferenceSystem targetCRS = CRS.decode("EPSG:4326");
        String msk50_1 = "PROJCS[\"МСК-50, зона 1\",GEOGCS[\"Pulkovo 1995\",DATUM[\"Pulkovo 1995\",SPHEROID[\"Krassowsky 1940\",6378245,298.3,AUTHORITY[\"EPSG\",\"7024\"]],TOWGS84[24.47,-130.89,-81.56,0,0,0.13,-0.22]],PRIMEM[\"Greenwich\",0],UNIT[\"degree\",0.0174532925199433]],UNIT[\"metre\",1],PROJECTION[\"Transverse_Mercator\"],PARAMETER[\"latitude_of_origin\",0],PARAMETER[\"central_meridian\",35.48333333333333],PARAMETER[\"scale_factor\",1],PARAMETER[\"false_easting\",1250000],PARAMETER[\"false_northing\",-5712900.566],AXIS[\"Y\",EAST],AXIS[\"X\",NORTH]]";
        String msk50_2 = "PROJCS[\"МСК-50, зона 2\",GEOGCS[\"Pulkovo 1995\",DATUM[\"Pulkovo 1995\",SPHEROID[\"Krassowsky 1940\",6378245,298.3,AUTHORITY[\"EPSG\",\"7024\"]],TOWGS84[24.47,-130.89,-81.56,0,0,0.13,-0.22]],PRIMEM[\"Greenwich\",0],UNIT[\"degree\",0.0174532925199433]],UNIT[\"metre\",1],PROJECTION[\"Transverse_Mercator\"],PARAMETER[\"latitude_of_origin\",0],PARAMETER[\"central_meridian\",38.48333333333333],PARAMETER[\"scale_factor\",1],PARAMETER[\"false_easting\",2250000],PARAMETER[\"false_northing\",-5712900.566],AXIS[\"Y\",EAST],AXIS[\"X\",NORTH]]";
        CoordinateReferenceSystem sourceCRS_1 = CRS.parseWKT(msk50_1);
        CoordinateReferenceSystem sourceCRS_2 = CRS.parseWKT(msk50_2);

        Map<String, ArrayList<LandProcessor>> sorted = new HashMap();
        for (LandProcessor lp : processors){
            if (!sorted.containsKey(lp.getSrs())) {
                sorted.put(lp.getSrs(), new ArrayList<LandProcessor>());
            }
            sorted.get(lp.getSrs()).add(lp);
        }
        List<LandProcessor> resultArr = new ArrayList<>();
        for (String key : sorted.keySet()){
            ArrayList<LandProcessor> pr = sorted.get(key);
            System.out.println("Сортировка по МСК, "+key);
            LandProcessor result = null;
            for (int i = 0; i < pr.size(); i++) {
                if (i == 0)
                    result = pr.get(i);
                else {
                    try {
                        result.mergeWith(pr.get(i));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
            // convert
            CoordinateReferenceSystem sourceCRS = null;
            if (key.equals("50.1")) {
                sourceCRS = sourceCRS_1;
            } else if (key.equals("50.2")) {
                sourceCRS = sourceCRS_2;
            }
            boolean lenient = true; // allow for some error due to different datums
            MathTransform transform = CRS.findMathTransform(sourceCRS, targetCRS, lenient);
            result.convertToWGS(transform);
            resultArr.add(result);
        }
        // merge wgs
        LandProcessor wgsResult = null;
        for (int i = 0; i < resultArr.size(); i++){
            if (i == 0)
                wgsResult = resultArr.get(i);
            else {
                try {
                    wgsResult.mergeWith(resultArr.get(i));
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        // save
        save(new File(savePath + "/convert", "convertMergedResultWGS84"), format, wgsResult);
    }

    private static void sortProcessorsAndSave(List<LandProcessor> processors, String savePath, String format) {
        Map<String, ArrayList<LandProcessor>> sorted = new HashMap();
        for (LandProcessor lp : processors){
           if (!sorted.containsKey(lp.getSrs())) {
               sorted.put(lp.getSrs(), new ArrayList<LandProcessor>());
           }
           sorted.get(lp.getSrs()).add(lp);
        }
        System.out.println(sorted.size());
        // merging
        for (String key : sorted.keySet()){
            ArrayList<LandProcessor> pr = sorted.get(key);
            System.out.println("Сортировка по МСК, "+key);
            LandProcessor result = null;
            for (int i = 0; i < pr.size(); i++) {
                if (i == 0)
                    result = pr.get(i);
                else {
                    try {
                        result.mergeWith(pr.get(i));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
            // save
            File checkDirectory = new File(savePath + "/convert/"+key);
            if (!checkDirectory.exists()) {
                checkDirectory.mkdir();
            }
            save(new File(savePath + "/convert/"+key, "convertMergedResult"), format, result);
        }

    }

    private static void save(File savedFile, String format, LandProcessor processor) {
        try {
            if ("shp".equals(format)) {
                processor.saveToShape(new File(savedFile.getPath()), "CP1251");
                saveNonSpatialData(savedFile, processor);
            }
            else if ("geojson".equals(format)) {
                processor.saveToGeeoJSON(new File(savedFile.getPath()), "UTF8");
                saveNonSpatialData(savedFile, processor);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private static void saveNonSpatialData(File savedFile, LandProcessor processor) {
        File landsFile = new File(savedFile + ".csv");
        try {
            landsFile.createNewFile();
        } catch (IOException e) {
            System.err.println(e.getMessage());
        }

        try(FileOutputStream fos=new FileOutputStream(landsFile))
        {
            for (String zu: processor.getNonSpatialParcels()) {
                byte[] buffer = zu.concat("\n").getBytes();
                fos.write(buffer, 0, buffer.length);
            }
        }
        catch(IOException ex){
            System.err.println(ex.getMessage());
        }

    }

    private static void usage() {
        System.err.println("Usage for example: java -Xmx2g -jar Landprocessor.10.x.x.jar {-merge} {-shp|-json} {-points} <input dir>"+
        "\nVersion: LandProcessor 10.11.23");
    }
}
