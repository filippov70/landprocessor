package org.tomskgislab.landprocessor;

import org.locationtech.jts.geom.*;
import org.locationtech.jts.geom.impl.CoordinateArraySequence;
import org.locationtech.jts.util.GeometricShapeFactory;
import extract.kpt.*;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.geotools.feature.DefaultFeatureCollection;
import org.geotools.feature.simple.SimpleFeatureBuilder;
import org.opengis.feature.simple.SimpleFeature;
import org.opengis.feature.simple.SimpleFeatureType;
import org.tomskgislab.landprocessor.constant.LandProcessorConstants;
import org.w3c.dom.Element;
import org.w3c.dom.Node;


public class CadastreEntitySpatial {
    //private static Logger logger = LogManager.getLogger(CadastreEntitySpatial.class);

    private EntitySpatialBound _entitySpatialBound = null;
    private EntitySpatialZUZacrep _entitySpatialZUOut = null;
    private EntitySpatialOKSOut _entitySpatialOKSOut = null;

    private Polygon polygon = null;
    private Polygon circle = null;
    private LineString line = null;
    private List<Polygon> polygonOKS = new ArrayList<Polygon>();
    //private List<Geometry> circleOKS = new ArrayList<Geometry>();
    private List<Point> charPoints = new ArrayList<Point>();
    private List<LineString> lineOKS = new ArrayList<LineString>();
    private GeometryFactory geometryFactory = null;

    private DefaultFeatureCollection charPointCollection;
    private SimpleFeatureBuilder featureBuilderCharPoint;

    List<LinearRing> rings;
    boolean isSavePoints;
    private String cadNumber;
    private String quartalNumber;


    // Constructors
    public CadastreEntitySpatial(EntitySpatialBound entitySpatialBound) {
        _entitySpatialBound = entitySpatialBound;
        parseEntitySpatial();
    }

    public CadastreEntitySpatial(EntitySpatialZUZacrep entitySpatialZUOut, boolean isSavePoints, String CadNum,
                                 String QuartalNum, DefaultFeatureCollection charPointCollection, SimpleFeatureBuilder featureBuilderCharPoint) {
        _entitySpatialZUOut = entitySpatialZUOut;
        this.cadNumber = CadNum;
        this.charPointCollection = charPointCollection;
        this.featureBuilderCharPoint = featureBuilderCharPoint;
        this.quartalNumber = QuartalNum;
        this.isSavePoints = isSavePoints;
        parseEntitySpatial();
    }

    public CadastreEntitySpatial(EntitySpatialOKSOut entitySpatialOKSOut, boolean isSavePoints, String CadNum, String QuartalNum) {
        _entitySpatialOKSOut = entitySpatialOKSOut;
        this.cadNumber = CadNum;
        this.quartalNumber = QuartalNum;
        this.isSavePoints = isSavePoints;
        parseEntitySpatial();
    }


    // Public members
    public Polygon getPolygon() {
        return polygon;
    }

//    public Geometry getCircle() {
//        return circle;
//    }

//    public Geometry getLine() {
//        return line;
//    }

    public List<Polygon> getPolygonOKS() {
        return polygonOKS;
    }

//    public List<Geometry> getCircleOKS() {
//        return circleOKS;
//    }

    public List<LineString> getLineOKS() {
        return lineOKS;
    }

    public GeometryFactory getGeometryFactory() {
        return geometryFactory;
    }


    // Private members
    private void parseEntitySpatial() {
        try {
            rings = new ArrayList<LinearRing>();
            // Кварталы, зоны, границы
            if (this._entitySpatialBound != null) {
                int spatialElementCount = _entitySpatialBound.getSpatialsElements().getSpatialElement().size();
                for (int i = 0; i < spatialElementCount; i++) {
                    CoordinateList coords = new CoordinateList();
                    int spElementUnitCount = _entitySpatialBound.getSpatialsElements().getSpatialElement().get(i).getOrdinates().getOrdinate().size();
                    for (int k = 0; k < spElementUnitCount; k++) {
                        OrdinateBound ordinateBound = _entitySpatialBound.getSpatialsElements().getSpatialElement().get(i).getOrdinates().getOrdinate().get(k);

                        double x = ordinateBound.getX().doubleValue();
                        double y = ordinateBound.getY().doubleValue();
                        Coordinate coord = new Coordinate(y, x);
                        coords.add(coord);
                    }
                    LinearRing r = createLinearRing(coords);
                    if (r != null) {
                        rings.add(r);
                    }
                }
                polygon = GeometryBuilder.createPolygon(rings);
            }

            if (this._entitySpatialZUOut != null) {

                int spatialElementCount = _entitySpatialZUOut.getSpatialsElements().getSpatialElement().size();
                for (int i = 0; i < spatialElementCount; i++) {
                    CoordinateList coords = new CoordinateList();
                    int spElementUnitCount = _entitySpatialZUOut.getSpatialsElements().getSpatialElement().get(i).getOrdinates().getOrdinate().size();
                    for (int k = 0; k < spElementUnitCount; k++) {
                        OrdinateZacrep ordinateBound = _entitySpatialZUOut.getSpatialsElements().getSpatialElement().get(i).getOrdinates().getOrdinate().get(k);
                        double x = ordinateBound.getX().doubleValue();
                        double y = ordinateBound.getY().doubleValue();
                        Coordinate coord = new Coordinate(y, x);
                        coords.add(coord);
                        if (isSavePoints) {
                            //featureBuilderCharPoint = new SimpleFeatureBuilder(this.CHAR_TYPE );
                            this.featureBuilderCharPoint.add(GeometryBuilder.createPoint(ordinateBound.getX(), ordinateBound.getY()));
                            this.featureBuilderCharPoint.add(ordinateBound.getNumGeopoint());
                            this.featureBuilderCharPoint.add(this.cadNumber);
                            this.featureBuilderCharPoint.add(this.quartalNumber);
                            String ord = LandProcessorConstants.NODATA;
                            if (ordinateBound.getOrdNmb() != null) {
                                ord =ordinateBound.getOrdNmb().toString();
                            }
                            this.featureBuilderCharPoint.add(ord);
                            String del = LandProcessorConstants.NODATA;
                            if (ordinateBound.getDeltaGeopoint() != null) {
                                del =ordinateBound.getDeltaGeopoint().toString();
                            }
                            this.featureBuilderCharPoint.add(del);

                            String zacr = LandProcessorConstants.NODATA;
                            if (ordinateBound.getGeopointZacrep() != null) {
                                zacr = ordinateBound.getGeopointZacrep();
                            }
                            this.featureBuilderCharPoint.add(zacr);
                            SimpleFeature point = this.featureBuilderCharPoint.buildFeature(null);
                            this.charPointCollection.add(point);
                        }
                    }
                    LinearRing r = createLinearRing(coords);
                    if (r != null) {
                        rings.add(r);
                    }
                }
                polygon = GeometryBuilder.createPolygon(rings);
            }

            if (this._entitySpatialOKSOut != null) {
                // ОКСы. По идее полигоны одноконтурные, но может состоять из нескольких объектов разного типа
                int spatialElementCount = _entitySpatialOKSOut.getSpatialsElements().getSpatialElement().size();
                for (int i = 0; i < spatialElementCount; i++) {
                    // Окружности
                    int spElementUnitCount = _entitySpatialOKSOut.getSpatialsElements().getSpatialElement().get(i).getOrdinates().getOrdinate().size();
                    BigDecimal r = _entitySpatialOKSOut.getSpatialsElements().getSpatialElement().get(i).getOrdinates().getOrdinate().get(0).getR();
                    if (r != null) {
                        GeometricShapeFactory shapeFactory = new GeometricShapeFactory();
                        shapeFactory.setNumPoints(64);
                        double x = _entitySpatialOKSOut.getSpatialsElements().getSpatialElement().get(i).getOrdinates().getOrdinate().get(0).getX().doubleValue();
                        double y = _entitySpatialOKSOut.getSpatialsElements().getSpatialElement().get(i).getOrdinates().getOrdinate().get(0).getY().doubleValue();
                        double radius = r.doubleValue();
                        shapeFactory.setCentre(new Coordinate(y, x));
                        shapeFactory.setSize(2.0 * radius);
                        circle = shapeFactory.createCircle();
                        polygonOKS.add(circle);
                        //logger.info("Circle processing");
                    }

                    int size = _entitySpatialOKSOut.getSpatialsElements().getSpatialElement().get(i).getOrdinates().getOrdinate().size();
                    OrdinateOKSOut firstOrd = _entitySpatialOKSOut.getSpatialsElements().getSpatialElement().get(i).getOrdinates().getOrdinate().get(0);
                    OrdinateOKSOut lastOrd = _entitySpatialOKSOut.getSpatialsElements().getSpatialElement().get(i).getOrdinates().getOrdinate().get(size-1);
                    String startNode = "";
                    String endNode = "";
                    if (firstOrd.getOrdNmb() != null && lastOrd.getOrdNmb() != null) {
                        startNode = firstOrd.getOrdNmb().toString();
                        endNode = lastOrd.getOrdNmb().toString();
                    } else { // искуственно задаём
                        System.out.println("Не заданы OrdNmb у точек! Расчёт по координатам");
                        if (firstOrd.getX() == lastOrd.getX() && firstOrd.getY() == lastOrd.getY()) {
                            startNode = "1";
                            endNode = "1";
                        } else {
                            startNode = "1";
                            endNode = "2";
                        }
                    }
                    // SuNumb - порядок обхода.
                    // полилинии
                    CoordinateList coords = new CoordinateList();
                    if (startNode.equals(endNode)) {
                        //List<CoordinateList> polylineList = new ArrayList<CoordinateList>();
                        int spElementUnitCountO = _entitySpatialOKSOut.getSpatialsElements().getSpatialElement().get(i).getOrdinates().getOrdinate().size();
                        for (int k = 0; k < spElementUnitCountO; k++) {
                            OrdinateOKSOut ordinateOks = _entitySpatialOKSOut.getSpatialsElements().getSpatialElement().get(i).getOrdinates().getOrdinate().get(k);
                            double x = ordinateOks.getX().doubleValue();
                            double y = ordinateOks.getY().doubleValue();
                            Coordinate coord = new Coordinate(y, x);
                            coords.add(coord);
                        }
                        //polylineList.add(coords);

                        line = GeometryBuilder.createLineString(coords);
                        lineOKS.add(line);
                        //logger.info("Line processing");
                    }
                    else { // полигоны
                        for (int k = 0; k < spElementUnitCount; k++) {
                            OrdinateOKSOut ordinateOks = _entitySpatialOKSOut.getSpatialsElements().getSpatialElement().get(i).getOrdinates().getOrdinate().get(k);
                            double x = ordinateOks.getX().doubleValue();
                            double y = ordinateOks.getY().doubleValue();
                            Coordinate coord = new Coordinate(y, x);
                            coords.add(coord);
                        }
                        LinearRing ri = createLinearRing(coords);
                        if (ri != null) {
                            rings.add(ri);
                        }

                        polygon = GeometryBuilder.createPolygon(rings);
                        polygonOKS.add(polygon);
                    }
                }

            }

        } catch (Exception ex) {
            //logger.error("parseEntitySpatial error: " + ex.getMessage());
            System.err.println("parseEntitySpatial error " + cadNumber + "; " + ex.getMessage());
        }
    }
    /**
     * Создаёт замкнутый конур на основе списка (CoordinateList) координат
     *
     * @return Контур JTS
     */
    private LinearRing createLinearRing(CoordinateList Coords) {
        try {
            if (Coords.size() < 3) {
                //logger.warn("Один из конутров объекта с КН " + cadNumber + " содержит менее 3-х точек! Контур не будет создан!");
                return null;
            } else {
                return GeometryBuilder.createLinearRing(Coords);
            }
        } catch (Exception ex) {
            //logger.error("createLinearRing " + ex);
            System.err.println(ex.getMessage() + "; createLinearRing " + cadNumber);
            return null;
        }
    }
}
