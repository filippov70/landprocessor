/*
LandProcessor - конвертер XML Росреестра в распространённые ГИС-форматы
Copyright 2011 Филиппов Владислав, aka nukevlad, aka filippov70
filippov70@gmail.com Россия, Томск, Кольцово
MIT License
Данная лицензия разрешает лицам, получившим копию данного программного
обеспечения и сопутствующей документации (в дальнейшем именуемыми «Программное Обеспечение»),
безвозмездно использовать Программное Обеспечение без ограничений, включая неограниченное
право на использование, копирование, изменение, слияние, публикацию, распространение,
сублицензирование и/или продажу копий Программного Обеспечения, а также лицам, которым
предоставляется данное Программное Обеспечение, при соблюдении следующих условий:
Указанное выше уведомление об авторском праве и данные условия должны быть включены во
все копии или значимые части данного Программного Обеспечения.

ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПРЕДОСТАВЛЯЕТСЯ «КАК ЕСТЬ», БЕЗ КАКИХ-ЛИБО ГАРАНТИЙ,
ЯВНО ВЫРАЖЕННЫХ ИЛИ ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ ГАРАНТИИ ТОВАРНОЙ ПРИГОДНОСТИ, СООТВЕТСТВИЯ
ПО ЕГО КОНКРЕТНОМУ НАЗНАЧЕНИЮ И ОТСУТСТВИЯ НАРУШЕНИЙ, НО НЕ ОГРАНИЧИВАЯСЬ ИМИ. НИ В КАКОМ
СЛУЧАЕ АВТОРЫ ИЛИ ПРАВООБЛАДАТЕЛИ НЕ НЕСУТ ОТВЕТСТВЕННОСТИ ПО КАКИМ-ЛИБО ИСКАМ, ЗА УЩЕРБ ИЛИ
ПО ИНЫМ ТРЕБОВАНИЯМ, В ТОМ ЧИСЛЕ, ПРИ ДЕЙСТВИИ КОНТРАКТА, ДЕЛИКТЕ ИЛИ ИНОЙ СИТУАЦИИ,
ВОЗНИКШИМ ИЗ-ЗА ИСПОЛЬЗОВАНИЯ ПРОГРАММНОГО ОБЕСПЕЧЕНИЯ ИЛИ ИНЫХ ДЕЙСТВИЙ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ.
 */

package org.tomskgislab.landprocessor.constant;

public class LandProcessorConstants {
  
    // Путь к словарям используемым в схемах
    // http://skipy-ru.livejournal.com/5343.html
    // http://spec-zone.ru/RU/Java/Docs/7/technotes/guides/lang/resources.html
    public static final String PATH_TO_XSD = "shema";
    
    public static final String KVZU = "KVZU";
    public static final String KPZU = "KPZU";
    public static final String KPT = "KPT";
    public static final String OKS = "OKS";
    public static final String KPOKS = "KPOKS";
    public static final String ZK = "Region_Cadastr";
    public static final String TP = "TP";
    
    
 // Словари
    public static final String dArea = "dArea_v01";
    public static final String dUnit = "dUnit_v01";
    public static final String dCategories = "dCategories_v01";
    public static final String dUtilizations = "dUtilizations_v01";
    public static final String dParcels = "dParcels_v01";
    public static final String dStates = "dStates_v01";
    public static final String dRights = "dRights_v02";
    public static final String dRealty = "dRealty_v03";
    public static final String dEncumbrances = "dEncumbrances_v03";
    public static final String dRegions = "dRegionsRF_v01";
    public static final String dAllDocuments = "dAllDocumentsOut_v03";
    public static final String dPermitUse = "dPermitUse_v01";
    public static final String dAllowedUse = "dAllowedUse_v02";

    public static final String NODATA = "--";
    
}//End of the class LandProcessorConstants
