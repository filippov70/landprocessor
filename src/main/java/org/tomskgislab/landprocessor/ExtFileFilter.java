/*
 * LandProcessor - конвертер XML Росреестра в Shape-файлы
 * Copyright 2011 Филиппов Владислав, aka nukevlad, aka filippov70
 * filippov70@gmail.com Россия, Томск
 * 
 * Этот файл — часть LandProcessor.

   LandProcessor - свободная программа: вы можете перераспространять её и/или
   изменять её на условиях Стандартной общественной лицензии GNU в том виде,
   в каком она была опубликована Фондом свободного программного обеспечения;
   либо версии 3 лицензии, либо (по вашему выбору) любой более поздней
   версии.

   LandProcessor распространяется в надежде, что она будет полезной,
   но БЕЗО ВСЯКИХ ГАРАНТИЙ; даже без неявной гарантии ТОВАРНОГО ВИДА
   или ПРИГОДНОСТИ ДЛЯ ОПРЕДЕЛЕННЫХ ЦЕЛЕЙ. Подробнее см. в Стандартной
   общественной лицензии GNU.

   Вы должны были получить копию Стандартной общественной лицензии GNU
   вместе с этой программой. Если это не так, см.
   <http://www.gnu.org/licenses/>.)
 */
package org.tomskgislab.landprocessor;

import java.io.File;

/**
 *
 * @author Филиппов Владислав
 */
public class ExtFileFilter extends javax.swing.filechooser.FileFilter {

      String ext;
      String description;

      public ExtFileFilter(String ext, String descr) {
          this.ext = ext;
          this.description = descr;
      }

      public boolean accept(File f) {
          if(f != null) {
              if(f.isDirectory()) {
                  return true;
              }
              String extension = getExtension(f);
              if( extension == null )
                  return (ext.length() == 0);
              return ext.equals(extension);
          }
          return false;
      }

      public String getExtension(File f) {
          if(f != null) {
              String filename = f.getName();
              int i = filename.lastIndexOf('.');
              if(i>0 && i<filename.length()-1) {
                  return filename.substring(i+1).toLowerCase();
              };
          }
          return null;
      }

      public String getDescription() {
          return description;
      }

}
