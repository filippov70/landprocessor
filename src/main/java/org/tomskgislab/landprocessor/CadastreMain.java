/*
 * LandProcessor - конвертер XML Росреестра в Shape-файлы
 * Copyright 2011 Филиппов Владислав, aka nukevlad, aka filippov70
 * filippov70@gmail.com Россия, Томск
 * 
 * Этот файл — часть LandProcessor.

   LandProcessor - свободная программа: вы можете перераспространять её и/или
   изменять её на условиях Стандартной общественной лицензии GNU в том виде,
   в каком она была опубликована Фондом свободного программного обеспечения;
   либо версии 3 лицензии, либо (по вашему выбору) любой более поздней
   версии.

   LandProcessor распространяется в надежде, что она будет полезной,
   но БЕЗО ВСЯКИХ ГАРАНТИЙ; даже без неявной гарантии ТОВАРНОГО ВИДА
   или ПРИГОДНОСТИ ДЛЯ ОПРЕДЕЛЕННЫХ ЦЕЛЕЙ. Подробнее см. в Стандартной
   общественной лицензии GNU.

   Вы должны были получить копию Стандартной общественной лицензии GNU
   вместе с этой программой. Если это не так, см.
   <http://www.gnu.org/licenses/>.)
 */
package org.tomskgislab.landprocessor;

import java.io.File;
import java.io.FileNotFoundException;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import org.locationtech.jts.geom.LineString;
import extract.kpt.*;

import org.geotools.data.simple.SimpleFeatureCollection;
import org.geotools.feature.DefaultFeatureCollection;
import org.geotools.feature.simple.SimpleFeatureBuilder;

//import org.geotools.referencing.crs.DefaultEngineeringCRS;

import org.opengis.feature.simple.SimpleFeature;
import org.opengis.feature.simple.SimpleFeatureType;
import org.opengis.referencing.crs.CoordinateReferenceSystem;

import org.tomskgislab.landprocessor.CadastreFeatureBuilder.CadastreFeatureTypes;

import org.locationtech.jts.geom.Polygon;
import org.tomskgislab.landprocessor.constant.LandProcessorConstants;
import org.tomskgislab.landprocessor.ex.LandProcessorException;

public class CadastreMain {

	//private static final Logger logger = LogManager.getLogger(CadastreMain.class);
	private DefaultFeatureCollection parcelCollection;
	private DefaultFeatureCollection quartalCollection;
	private DefaultFeatureCollection zoneCollection;
	private DefaultFeatureCollection boundCollection;
	private DefaultFeatureCollection realtyCollection;
	private DefaultFeatureCollection realtyLineCollection;
	private DefaultFeatureCollection OMSCollection;
	private DefaultFeatureCollection charPointCollection;
	private SimpleFeatureType PARCEL_TYPE;
	private SimpleFeatureType QUARTAL_TYPE;
	private SimpleFeatureType ZONE_TYPE;
	private SimpleFeatureType BOUND_TYPE;
	private SimpleFeatureType REALTY_TYPE;
	private SimpleFeatureType REALTYLINE_TYPE;
	private SimpleFeatureType OMS_TYPE;
	private SimpleFeatureType CHAR_TYPE;
	private boolean savePoints = false;
	private String quartalSpatialSystem = "";
	private String srs = "-";

	private List<String> nonSpatialParcels = new ArrayList<>();

	private ExtractCadastralPlanTerritory kpt = null; // КПТ

	public CadastreMain() throws Exception {

		// One constant deserves special mention as it is used as a “wild card”
		// placeholder for when you are unsure of your data. The concept of a
		// “Generic 2D”
		// CoordianteReferenceSystem is formally intended for working with
		// things
		// like CAD drawings where the results are measured in meters.
		//CoordinateReferenceSystem crs = DefaultEngineeringCRS.GENERIC_2D;// (EPSG,
																			// false);

		PARCEL_TYPE = CadastreFeatureBuilder.createFeatureType(CadastreFeatureTypes.Parcel);
		QUARTAL_TYPE = CadastreFeatureBuilder.createFeatureType( CadastreFeatureTypes.Quartal);
		ZONE_TYPE = CadastreFeatureBuilder.createFeatureType( CadastreFeatureTypes.Zones);
		BOUND_TYPE = CadastreFeatureBuilder.createFeatureType( CadastreFeatureTypes.Bounds);
		REALTY_TYPE = CadastreFeatureBuilder.createFeatureType( CadastreFeatureTypes.Realty);
		REALTYLINE_TYPE = CadastreFeatureBuilder.createFeatureType(CadastreFeatureTypes.RealtyLine);
		OMS_TYPE = CadastreFeatureBuilder.createFeatureType(CadastreFeatureTypes.OMSPoint);
		CHAR_TYPE = CadastreFeatureBuilder.createFeatureType(CadastreFeatureTypes.CharacterPoint);

		parcelCollection = new DefaultFeatureCollection("internal", PARCEL_TYPE);
		quartalCollection = new DefaultFeatureCollection("internal", QUARTAL_TYPE);
		zoneCollection = new DefaultFeatureCollection("internal", ZONE_TYPE);
		boundCollection = new DefaultFeatureCollection("internal", BOUND_TYPE);
		realtyCollection = new DefaultFeatureCollection("internal", REALTY_TYPE);
		realtyLineCollection = new DefaultFeatureCollection("internal", REALTYLINE_TYPE);
		OMSCollection = new DefaultFeatureCollection("internal", OMS_TYPE);
		charPointCollection = new DefaultFeatureCollection("internal", CHAR_TYPE);
	}

	public void parse(File PathToFile, boolean isSavePoints) {

		if (isSavePoints == true) {
			this.savePoints = true;
		}
//
//		DocumentBuilderFactory domFactory = DocumentBuilderFactory.newInstance();
//		domFactory.setNamespaceAware(true); // never forget this!
//		DocumentBuilder builder = null;

		try {
			Scanner scan = new Scanner(PathToFile);
			String currLine;
			//while (scan.hasNext() && (currLine = scan.nextLine()) != null) {
				//scan.findInLine()
				//if (currLine.contains("extract_cadastral_plan_territory") == true) {
					parseQuartalExtract(PathToFile);
				//	break;
				//}
			//}
		} catch (FileNotFoundException ex){
			System.err.println("File not found "+PathToFile);
		} catch (Exception ex) {
			System.err.println("Error in parse method, "+PathToFile +". "+ex.getMessage());
		}

	}

	public List<String> getNonSpatialParcels() {
		return this.nonSpatialParcels;
	}

	public DefaultFeatureCollection getParcelCollection() {
		return parcelCollection;
	}

	public DefaultFeatureCollection getQuartalCollection() {
		return quartalCollection;
	}

	public DefaultFeatureCollection getZoneCollection() {
		return zoneCollection;
	}

	public DefaultFeatureCollection getBoundCollection() {
		return boundCollection;
	}

	public DefaultFeatureCollection getRealtyCollection() {
		return realtyCollection;
	}

	public DefaultFeatureCollection getRealtyLineCollection() {
		return realtyLineCollection;
	}

	public DefaultFeatureCollection getOMSCollection() {
		return OMSCollection;
	}

	public DefaultFeatureCollection getCharPointCollection() {
		return charPointCollection;
	}

	public SimpleFeatureType getPARCEL_TYPE() {
		return PARCEL_TYPE;
	}

	public SimpleFeatureType getQUARTAL_TYPE() {
		return QUARTAL_TYPE;
	}

	public SimpleFeatureType getZONEL_TYPE() {
		return ZONE_TYPE;
	}

	public SimpleFeatureType getBOUND_TYPE() {
		return BOUND_TYPE;
	}

	public SimpleFeatureType getREALTY_TYPE() {
		return REALTY_TYPE;
	}

	public SimpleFeatureType getREALTYLINE_TYPE() {
		return REALTYLINE_TYPE;
	}

	public SimpleFeatureType getOMS_TYPE() {
		return OMS_TYPE;
	}

	public SimpleFeatureType getCHAR_TYPE() {
		return CHAR_TYPE;
	}

	public String getSrs() {
		return this.srs;
	}

	private void parseQuartalExtract(File PathToFile) {
		try {
			SimpleFeatureBuilder featureBuilderParcel = new SimpleFeatureBuilder(PARCEL_TYPE);
			SimpleFeatureBuilder featureBuilderQuartal = new SimpleFeatureBuilder(QUARTAL_TYPE);
			SimpleFeatureBuilder featureBuilderZone = new SimpleFeatureBuilder(ZONE_TYPE);
			SimpleFeatureBuilder featureBuilderBound = new SimpleFeatureBuilder(BOUND_TYPE);
			SimpleFeatureBuilder featureBuilderRealty = new SimpleFeatureBuilder(REALTY_TYPE);
			SimpleFeatureBuilder featureBuilderRealtyLine = new SimpleFeatureBuilder(REALTYLINE_TYPE);
			SimpleFeatureBuilder featureBuilderOMS = new SimpleFeatureBuilder(OMS_TYPE);
			SimpleFeatureBuilder featureBuilderCharPoint = new SimpleFeatureBuilder(CHAR_TYPE);

			JAXBContext context = JAXBContext.newInstance(ObjectFactory.class);
			Unmarshaller unmarshaller = context.createUnmarshaller();
			//Object obj = unmarshaller.unmarshal(PathToFile);
			JAXBElement<ExtractCadastralPlanTerritory> ecpt = (JAXBElement<ExtractCadastralPlanTerritory>)unmarshaller.unmarshal(PathToFile);
			kpt = ecpt.getValue();

			if (kpt != null) {
				System.out.println("Processing quartal " + kpt.getCadastralBlocks().getCadastralBlock().get(0).getCadastralNumber());
				//this.nonSpatialParcels = new ArrayList<>();
				if (kpt.getCadastralBlocks() != null) {
					for (int i = 0; i < kpt.getCadastralBlocks().getCadastralBlock().size(); i++) {
						CadastralBlock block = kpt.getCadastralBlocks().getCadastralBlock().get(i);
						String quartalNumber = LandProcessorConstants.NODATA;
						String respDate = kpt.getDetailsRequest().getDateReceivedRequest().toString();

                        String cn = "?"; // кадастровый номер для записи в лог при эксепшене
						// Quartal
                        try {
                            System.out.println("Processing Quartal");
                            EntitySpatialBound blockSpatial = block.getSpatialData().getEntitySpatial();
                            String blockCadNumber = block.getCadastralNumber();
                            quartalNumber = blockCadNumber;
                            cn = blockCadNumber;
                            String spatialSystem = block.getSpatialData().getEntitySpatial().getSkId();
                            CadastreEntitySpatial ces = new CadastreEntitySpatial(blockSpatial);
                            Polygon boundGeom = ces.getPolygon();
                            if (boundGeom != null) {
								featureBuilderQuartal.add(boundGeom);
								featureBuilderQuartal.add(blockCadNumber);
                                featureBuilderQuartal.add(block.getAreaQuarter().getArea().doubleValue());
                                featureBuilderQuartal.add(LandProcessorConstants.NODATA);
                                featureBuilderQuartal.add(blockSpatial.getSkId());
                                featureBuilderQuartal.add(blockCadNumber);
                                featureBuilderQuartal.add(respDate);
								featureBuilderQuartal.add(block.getSpatialData().getEntitySpatial().getSkId());
								this.srs = block.getSpatialData().getEntitySpatial().getSkId();
								SimpleFeature quartal = featureBuilderQuartal.buildFeature(null);
								quartalCollection.add(quartal);
                            }

                        } catch (Exception ex){
                            System.err.println(ex.getLocalizedMessage() + ";" + cn);
                        }
						System.out.println("Finish Quartal");
						// Bounds
						System.out.println("Processing Bounds...");
                        try {
                            if (block.getSubjectBoundaries() != null) {
                                for (int j = 0; j < block.getSubjectBoundaries().getSubjectBoundaryRecord().size(); j++) {
                                    BoundContoursOut bCntrs = block.getSubjectBoundaries().getSubjectBoundaryRecord().get(j).getBContoursLocation().getContours();
                                    String descrS = LandProcessorConstants.NODATA;
									String regNumbBorder = LandProcessorConstants.NODATA;
									String typeBoundary = LandProcessorConstants.NODATA;
									String regDate = LandProcessorConstants.NODATA;
									if (block.getSubjectBoundaries().getSubjectBoundaryRecord().get(j).getBObjectSubjectBoundary() != null) {
										if (block.getSubjectBoundaries().getSubjectBoundaryRecord().get(j).getBObjectSubjectBoundary().getDescription() != null)
											descrS = block.getSubjectBoundaries().getSubjectBoundaryRecord().get(j).getBObjectSubjectBoundary().getDescription();

										if (block.getSubjectBoundaries().getSubjectBoundaryRecord().get(j).getBObjectSubjectBoundary().getBObject().getRegNumbBorder() != null)
											regNumbBorder = block.getSubjectBoundaries().getSubjectBoundaryRecord().get(j).getBObjectSubjectBoundary().getBObject().getRegNumbBorder();

										if (block.getSubjectBoundaries().getSubjectBoundaryRecord().get(j).getBObjectSubjectBoundary().getBObject().getTypeBoundary().getValue() != null)
											typeBoundary = block.getSubjectBoundaries().getSubjectBoundaryRecord().get(j).getBObjectSubjectBoundary().getBObject().getTypeBoundary().getValue();

										if (block.getSubjectBoundaries().getSubjectBoundaryRecord().get(j).getRecordInfo().getRegistrationDate().toString() != null)
											regDate = block.getSubjectBoundaries().getSubjectBoundaryRecord().get(j).getRecordInfo().getRegistrationDate().toString();
									}
                                    makeGeometryByESBoundList(bCntrs, featureBuilderBound, descrS, "Субъекты РФ", regNumbBorder, typeBoundary, regDate, quartalNumber, respDate);
                                }
                            }
                            if (block.getMunicipalBoundaries() != null) {
                                for (int j = 0; j < block.getMunicipalBoundaries().getMunicipalBoundaryRecord().size(); j++) {
                                    BoundContoursOut bCntrs = block.getMunicipalBoundaries().getMunicipalBoundaryRecord().get(j).getBContoursLocation().getContours();
                                    String descrM = LandProcessorConstants.NODATA;
									String regNumbBorder = LandProcessorConstants.NODATA;
									String typeBoundary = LandProcessorConstants.NODATA;
									String regDate = LandProcessorConstants.NODATA;
									if (block.getMunicipalBoundaries().getMunicipalBoundaryRecord().get(j).getBObjectMunicipalBoundary() != null) {
										if (block.getMunicipalBoundaries().getMunicipalBoundaryRecord().get(j).getBObjectMunicipalBoundary().getDescription() != null)
											descrM = block.getMunicipalBoundaries().getMunicipalBoundaryRecord().get(j).getBObjectMunicipalBoundary().getDescription();

										if (block.getMunicipalBoundaries().getMunicipalBoundaryRecord().get(j).getBObjectMunicipalBoundary().getBObject().getRegNumbBorder() != null)
											regNumbBorder = block.getMunicipalBoundaries().getMunicipalBoundaryRecord().get(j).getBObjectMunicipalBoundary().getBObject().getRegNumbBorder();

										if (block.getMunicipalBoundaries().getMunicipalBoundaryRecord().get(j).getBObjectMunicipalBoundary().getBObject().getTypeBoundary().getValue() != null)
											typeBoundary = block.getMunicipalBoundaries().getMunicipalBoundaryRecord().get(j).getBObjectMunicipalBoundary().getBObject().getTypeBoundary().getValue();

										if (block.getMunicipalBoundaries().getMunicipalBoundaryRecord().get(j).getRecordInfo().getRegistrationDate().toString() != null)
											regDate = block.getMunicipalBoundaries().getMunicipalBoundaryRecord().get(j).getRecordInfo().getRegistrationDate().toString();
									}
                                    makeGeometryByESBoundList(bCntrs, featureBuilderBound, descrM, "Муниципалитеты", regNumbBorder, typeBoundary, regDate, quartalNumber, respDate);
                                }
                            }
                            if (block.getInhabitedLocalityBoundaries() != null) {
                                for (int j = 0; j < block.getInhabitedLocalityBoundaries().getInhabitedLocalityBoundaryRecord().size(); j++) {
                                    BoundContoursOut bCntrs = block.getInhabitedLocalityBoundaries().getInhabitedLocalityBoundaryRecord().get(j).getBContoursLocation().getContours();
                                    String descrI = LandProcessorConstants.NODATA;
									String regNumbBorder = LandProcessorConstants.NODATA;
									String typeBoundary = LandProcessorConstants.NODATA;
									String regDate = LandProcessorConstants.NODATA;
									if (block.getInhabitedLocalityBoundaries().getInhabitedLocalityBoundaryRecord().get(j).getBObjectInhabitedLocalityBoundary() != null) {
										if (block.getInhabitedLocalityBoundaries().getInhabitedLocalityBoundaryRecord().get(j).getBObjectInhabitedLocalityBoundary().getDescription() != null)
											descrI = block.getInhabitedLocalityBoundaries().getInhabitedLocalityBoundaryRecord().get(j).getBObjectInhabitedLocalityBoundary().getDescription();

										if (block.getInhabitedLocalityBoundaries().getInhabitedLocalityBoundaryRecord().get(j).getBObjectInhabitedLocalityBoundary().getBObject().getRegNumbBorder() != null)
											regNumbBorder = block.getInhabitedLocalityBoundaries().getInhabitedLocalityBoundaryRecord().get(j).getBObjectInhabitedLocalityBoundary().getBObject().getRegNumbBorder();

										if (block.getInhabitedLocalityBoundaries().getInhabitedLocalityBoundaryRecord().get(j).getBObjectInhabitedLocalityBoundary().getBObject().getTypeBoundary().getValue() != null)
											typeBoundary = block.getInhabitedLocalityBoundaries().getInhabitedLocalityBoundaryRecord().get(j).getBObjectInhabitedLocalityBoundary().getBObject().getTypeBoundary().getValue();

										if (block.getInhabitedLocalityBoundaries().getInhabitedLocalityBoundaryRecord().get(j).getRecordInfo().getRegistrationDate().toString() != null)
											regDate = block.getInhabitedLocalityBoundaries().getInhabitedLocalityBoundaryRecord().get(j).getRecordInfo().getRegistrationDate().toString();
									}
                                    makeGeometryByESBoundList(bCntrs, featureBuilderBound, descrI, "Населённые пункты", regNumbBorder, typeBoundary, regDate, quartalNumber, respDate);
                                }
                            }
                        } catch (Exception ex) {
                            System.err.println("Boundaries Error;" + ex.getLocalizedMessage());
                        }
						System.out.println("Finish Bounds");
						System.out.println("Processing Zones...");
						// Zones
						if (block.getZonesAndTerritoriesBoundaries() != null) {
							ZonesAndTerritoriesBoundariesType zonesList = block.getZonesAndTerritoriesBoundaries();
							for (int j = 0; j < zonesList.getZonesAndTerritoriesRecord().size(); j++) {
								BoundContoursOut bCntrs = zonesList.getZonesAndTerritoriesRecord().get(j).getBContoursLocation().getContours();
								BobjectZonesAndTerritories record = zonesList.getZonesAndTerritoriesRecord().get(j).getBObjectZonesAndTerritories();
								String recordName = LandProcessorConstants.NODATA;
								String recordDescr = LandProcessorConstants.NODATA;
								String recordNumber = LandProcessorConstants.NODATA;
								String recordType = LandProcessorConstants.NODATA;
								String recordOther = LandProcessorConstants.NODATA;
								String recordRegNumb = LandProcessorConstants.NODATA;
								String recordTypeBoundary = LandProcessorConstants.NODATA;

								if (record.getNameByDoc() != null)
									recordName = record.getNameByDoc();
								if (record.getDescription() != null)
									recordDescr = record.getDescription();
								if (record.getNumber() != null)
									recordNumber = record.getNumber();
								if (record.getTypeZone() != null)
									recordType = record.getTypeZone().getValue();
								if (record.getOther() != null)
									recordOther = record.getOther();
								if (record.getBObject() != null && record.getBObject().getRegNumbBorder() != null)
									recordRegNumb = record.getBObject().getRegNumbBorder();
								if (record.getBObject() != null && record.getBObject().getTypeBoundary() != null)
									recordTypeBoundary = record.getBObject().getTypeBoundary().getValue();

								for (int k = 0; k < bCntrs.getContour().size(); k++) {
									CadastreEntitySpatial ces = new CadastreEntitySpatial(bCntrs.getContour().get(k).getEntitySpatial());
									Polygon g = ces.getPolygon();
                                    if (g != null) {
                                        try {
											featureBuilderZone.add(g);
											featureBuilderZone.add(recordName);
											featureBuilderZone.add(recordNumber);
											featureBuilderZone.add(recordDescr);
											featureBuilderZone.add(recordType);
											featureBuilderZone.add(recordRegNumb);
											featureBuilderZone.add(recordTypeBoundary);
											featureBuilderZone.add(recordOther);
                                            featureBuilderZone.add(quartalNumber);
                                            featureBuilderZone.add(respDate);

											SimpleFeature zone = featureBuilderZone.buildFeature(null);
											zoneCollection.add(zone);
                                        } catch (Exception ex) {
                                            System.err.println("Zones Error;" + ex.getMessage());
                                        }
                                    }
								}
							}
						}

						System.out.println("Finish Zones");

						System.out.println("Processing realty...");
						if (block.getRecordData() != null) {
							// Parcels
							if (block.getRecordData().getBaseData().getLandRecords() != null) {
								System.out.println("Processing parcels...");
								BaseDataType.LandRecords land = block.getRecordData().getBaseData().getLandRecords();
								for (int j = 0; j < land.getLandRecord().size(); j++) {
								    cn = "?";
									if (land.getLandRecord().get(j).getContoursLocation() != null) {
										for (int k = 0; k < land.getLandRecord().get(j).getContoursLocation().getContours().getContour().size(); k++) {
											CadastreEntitySpatial ces = new CadastreEntitySpatial(land.getLandRecord().get(j).getContoursLocation().getContours().getContour().get(k).getEntitySpatial(),
												this.savePoints, getCadNum(land.getLandRecord().get(j)), quartalNumber, charPointCollection, featureBuilderCharPoint);
                                            if (ces.getPolygon() != null) { //
                                                try {
                                                    Polygon g = ces.getPolygon();
                                                    featureBuilderParcel.add(g);

													//featureBuilderParcel.add("ЗУ");
                                                    featureBuilderParcel.add(quartalNumber);
													featureBuilderParcel.add(getCadNum(land.getLandRecord().get(j)));
													cn = getCadNum(land.getLandRecord().get(j));
													featureBuilderParcel.add(getCommonCadNum(land.getLandRecord().get(j)));

													featureBuilderParcel.add(getCategory(land.getLandRecord().get(j)));

													featureBuilderParcel.add(getPermUseByDoc(land.getLandRecord().get(j)));
													featureBuilderParcel.add(getPermUseByType(land.getLandRecord().get(j)));
													featureBuilderParcel.add(getPermUseByGradReg(land.getLandRecord().get(j)));

													featureBuilderParcel.add(getSubType(land.getLandRecord().get(j)));
													featureBuilderParcel.add(getType(land.getLandRecord().get(j)));
													featureBuilderParcel.add(getArea(land.getLandRecord().get(j)));
                                                    featureBuilderParcel.add(getAreaInaccur(land.getLandRecord().get(j)));

													featureBuilderParcel.add(getAdrType(land.getLandRecord().get(j)));
													featureBuilderParcel.add(getLocation(land.getLandRecord().get(j)));
													featureBuilderParcel.add(getAdr1(land.getLandRecord().get(j)));

													featureBuilderParcel.add(getStreet(land.getLandRecord().get(j)));
													featureBuilderParcel.add(getLeve1(land.getLandRecord().get(j)));
													featureBuilderParcel.add(getLeve2(land.getLandRecord().get(j)));
													featureBuilderParcel.add(getLeve3(land.getLandRecord().get(j)));
													featureBuilderParcel.add(getAppart(land.getLandRecord().get(j)));

													featureBuilderParcel.add(getOther(land.getLandRecord().get(j)));
													featureBuilderParcel.add(getNote(land.getLandRecord().get(j)));
                                                    featureBuilderParcel.add(respDate);
													SimpleFeature parcel = featureBuilderParcel.buildFeature(null);
													parcelCollection.add(parcel);
													if (this.savePoints) {

													}

                                                } catch (Exception ex) {
                                                    System.err.println("Error in create Parcel; " + cn + ";" + ex.getMessage());
                                                }
                                            } else {

											}
										}
									} else {
										String data = "";
										data += quartalNumber + ";";
										data += getCadNum(land.getLandRecord().get(j)) + ";";
										data += getCommonCadNum(land.getLandRecord().get(j)) + ";";

										data += getCategory(land.getLandRecord().get(j)) + ";";

										data += getPermUseByDoc(land.getLandRecord().get(j)) + ";";
										data += getPermUseByType(land.getLandRecord().get(j)) + ";";
										data += getPermUseByGradReg(land.getLandRecord().get(j)) + ";";

										data += getSubType(land.getLandRecord().get(j)) + ";";
										data += getType(land.getLandRecord().get(j)) + ";";
										data += getArea(land.getLandRecord().get(j)) + ";";
										data += getAreaInaccur(land.getLandRecord().get(j)) + ";";

										data += getAdrType(land.getLandRecord().get(j)) + ";";
										data += getLocation(land.getLandRecord().get(j)) + ";";
										data += getAdr1(land.getLandRecord().get(j)) + ";";

										data += getStreet(land.getLandRecord().get(j)) + ";";
										data += getLeve1(land.getLandRecord().get(j)) + ";";
										data += getLeve2(land.getLandRecord().get(j)) + ";";
										data += getLeve3(land.getLandRecord().get(j)) + ";";
										data += getAppart(land.getLandRecord().get(j)) + ";";

										data += getOther(land.getLandRecord().get(j)) + ";";
										data += getNote(land.getLandRecord().get(j)) ;
										this.nonSpatialParcels.add(data);
									}
								}
							}
							// OKS build
							if (block.getRecordData().getBaseData().getBuildRecords() != null) {
								System.out.println("Processing buildings...");
								BaseDataType.BuildRecords build = block.getRecordData().getBaseData().getBuildRecords();
								for (int j = 0; j < build.getBuildRecord().size(); j++) {
								    cn = "?";
									if (build.getBuildRecord().get(j).getContours() != null) {
										for (int k = 0; k < build.getBuildRecord().get(j).getContours().getContour().size(); k++) {
											//System.out.println("Geometry with " + build.getBuildRecord().get(j).getContours().getContour().size() + " contours. " + getCadNum(build.getBuildRecord().get(j)));
											CadastreEntitySpatial ces = new CadastreEntitySpatial(build.getBuildRecord().get(j).getContours().getContour().get(k).getEntitySpatial(), false, "", "");
                                            if (ces.getPolygonOKS().size() != 0) {
                                                List<Polygon> gbl = ces.getPolygonOKS();
                                                for (int h=0; h < gbl.size(); h++) {
                                                    try {
														if (gbl.get(h)!=null) {
															featureBuilderRealty.add(gbl.get(h));
															featureBuilderRealty.add("Строение");
															featureBuilderRealty.add(getPurpose(build.getBuildRecord().get(j)));
															featureBuilderRealty.add(getCadNum(build.getBuildRecord().get(j)));
															cn = getCadNum(build.getBuildRecord().get(j));
															featureBuilderRealty.add(getPermittedUses(build.getBuildRecord().get(j)));
															featureBuilderRealty.add(LandProcessorConstants.NODATA);
															featureBuilderRealty.add(getArea(build.getBuildRecord().get(j)));
															featureBuilderRealty.add(getAdr1(build.getBuildRecord().get(j)));
															featureBuilderRealty.add(getStreet(build.getBuildRecord().get(j)));
															featureBuilderRealty.add(getLeve1(build.getBuildRecord().get(j)));
															featureBuilderRealty.add(getLeve2(build.getBuildRecord().get(j)));
															featureBuilderRealty.add(getLeve3(build.getBuildRecord().get(j)));
															featureBuilderRealty.add(getAppart(build.getBuildRecord().get(j)));
															featureBuilderRealty.add(getOther(build.getBuildRecord().get(j)));
															featureBuilderRealty.add(getNote(build.getBuildRecord().get(j)));
															SimpleFeature buildP = featureBuilderRealty.buildFeature(null);
															realtyCollection.add(buildP);
														}
                                                    } catch (Exception ex) {
                                                        System.err.println("Error in create Building; " + cn + ";" + ex.getMessage());
                                                    }
                                                }
                                            }
                                            if (ces.getLineOKS().size() != 0) {
                                                List<LineString> gbl = ces.getLineOKS();
                                                for (int h=0; h < gbl.size(); h++) {
                                                    try {
														if (gbl.get(h) != null) {
															featureBuilderRealtyLine.add(gbl.get(h));
															featureBuilderRealtyLine.add("Строение линейное");
															featureBuilderRealtyLine.add(getPurpose(build.getBuildRecord().get(j)));
															featureBuilderRealtyLine.add(getCadNum(build.getBuildRecord().get(j)));
                                                            cn = getCadNum(build.getBuildRecord().get(j));
															featureBuilderRealtyLine.add(getPermittedUses(build.getBuildRecord().get(j)));
															featureBuilderRealtyLine.add(LandProcessorConstants.NODATA);
															featureBuilderRealtyLine.add(0.0);
															featureBuilderRealtyLine.add(getAdr1(build.getBuildRecord().get(j)));
															featureBuilderRealtyLine.add(getStreet(build.getBuildRecord().get(j)));
															featureBuilderRealtyLine.add(getLeve1(build.getBuildRecord().get(j)));
															featureBuilderRealtyLine.add(getLeve2(build.getBuildRecord().get(j)));
															featureBuilderRealtyLine.add(getLeve3(build.getBuildRecord().get(j)));
															featureBuilderRealtyLine.add(getAppart(build.getBuildRecord().get(j)));
															featureBuilderRealtyLine.add(getOther(build.getBuildRecord().get(j)));
															featureBuilderRealtyLine.add(getNote(build.getBuildRecord().get(j)));
															SimpleFeature buildL = featureBuilderRealtyLine.buildFeature(null);
															realtyLineCollection.add(buildL);
														}
                                                    } catch (Exception ex) {
                                                        System.err.println("Error in create Building Line; " + cn + ";" + ex.getMessage());
                                                    }

                                                }
                                            }
										}
									}
								}

							}
							System.out.println("Processing constructions...");
							// OKS construction
							if (block.getRecordData().getBaseData().getConstructionRecords() != null) {
								BaseDataType.ConstructionRecords contr = block.getRecordData().getBaseData().getConstructionRecords();
								for (int j = 0; j < contr.getConstructionRecord().size(); j++) {
								    cn = "?";
									if (contr.getConstructionRecord().get(j).getContours() != null) {
										for (int k = 0; k < contr.getConstructionRecord().get(j).getContours().getContour().size(); k++) {
											//System.out.println("Geometry with " + contr.getConstructionRecord().get(j).getContours().getContour().size() + " contours " + getCadNum(contr.getConstructionRecord().get(j)));
											CadastreEntitySpatial ces = new CadastreEntitySpatial(contr.getConstructionRecord().get(j).getContours().getContour().get(k).getEntitySpatial(), false, "", "");
											if (ces.getPolygonOKS().size() != 0) {
												List<Polygon> gbl = ces.getPolygonOKS();
												for (int h=0; h < gbl.size(); h++) {
													try {
														if (gbl.get(h)!=null) {
															featureBuilderRealty.add(gbl.get(h));
															featureBuilderRealty.add("Сооружение");
															featureBuilderRealty.add(getPurpose(contr.getConstructionRecord().get(j)));
															featureBuilderRealty.add(getCadNum(contr.getConstructionRecord().get(j)));
															cn = getCadNum(contr.getConstructionRecord().get(j));
															featureBuilderRealty.add(getPermittedUses(contr.getConstructionRecord().get(j)));
															featureBuilderRealty.add(LandProcessorConstants.NODATA);
															featureBuilderRealty.add(0.0);
															featureBuilderRealty.add(getAdr1(contr.getConstructionRecord().get(j)));
															featureBuilderRealty.add(getStreet(contr.getConstructionRecord().get(j)));
															featureBuilderRealty.add(getLeve1(contr.getConstructionRecord().get(j)));
															featureBuilderRealty.add(getLeve2(contr.getConstructionRecord().get(j)));
															featureBuilderRealty.add(getLeve3(contr.getConstructionRecord().get(j)));
															featureBuilderRealty.add(getAppart(contr.getConstructionRecord().get(j)));
															featureBuilderRealty.add(getOther(contr.getConstructionRecord().get(j)));
															featureBuilderRealty.add(getNote(contr.getConstructionRecord().get(j)));
															SimpleFeature buildP = featureBuilderRealty.buildFeature(null);
															realtyCollection.add(buildP);
														}
													} catch (Exception ex) {
														System.err.println("Error in create constructions; " + cn + ";" + ex.getMessage());
													}
												}
											}
											if (ces.getLineOKS().size() != 0) {
												List<LineString> gbl = ces.getLineOKS();
												for (int h=0; h < gbl.size(); h++) {
													try {
														if (gbl.get(h)!=null) {
															featureBuilderRealtyLine.add(gbl.get(h));
															featureBuilderRealtyLine.add("Сооружение линейное");
															featureBuilderRealtyLine.add(getPurpose(contr.getConstructionRecord().get(j)));
															featureBuilderRealtyLine.add(getCadNum(contr.getConstructionRecord().get(j)));
                                                            cn = getCadNum(contr.getConstructionRecord().get(j));
															featureBuilderRealtyLine.add(getPermittedUses(contr.getConstructionRecord().get(j)));
															featureBuilderRealtyLine.add(LandProcessorConstants.NODATA);
															featureBuilderRealtyLine.add(0.0);
															featureBuilderRealtyLine.add(getAdr1(contr.getConstructionRecord().get(j)));
															featureBuilderRealtyLine.add(getStreet(contr.getConstructionRecord().get(j)));
															featureBuilderRealtyLine.add(getLeve1(contr.getConstructionRecord().get(j)));
															featureBuilderRealtyLine.add(getLeve2(contr.getConstructionRecord().get(j)));
															featureBuilderRealtyLine.add(getLeve3(contr.getConstructionRecord().get(j)));
															featureBuilderRealtyLine.add(getAppart(contr.getConstructionRecord().get(j)));
															featureBuilderRealtyLine.add(getOther(contr.getConstructionRecord().get(j)));
															featureBuilderRealtyLine.add(getNote(contr.getConstructionRecord().get(j)));
															SimpleFeature buildL = featureBuilderRealtyLine.buildFeature(null);
															realtyLineCollection.add(buildL);
														}
													} catch (Exception ex) {
														System.err.println("Error in create constructions Line; " + cn + ";" + ex.getMessage());
													}

												}
											}
										}

									}
								}

								System.out.println("Processing under constructions...");
								// OKS under contsruction
								if (block.getRecordData().getBaseData().getObjectUnderConstructionRecords() != null) {
									BaseDataType.ObjectUnderConstructionRecords under = block.getRecordData().getBaseData().getObjectUnderConstructionRecords();
									for (int j = 0; j < under.getObjectUnderConstructionRecord().size(); j++) {
									    cn = "?";
										if (under.getObjectUnderConstructionRecord().get(j).getContours() != null) {
											for (int k = 0; k < under.getObjectUnderConstructionRecord().get(j).getContours().getContour().size(); k++) {
												CadastreEntitySpatial ces = new CadastreEntitySpatial(under.getObjectUnderConstructionRecord().get(j).getContours().getContour().get(k).getEntitySpatial(), false, "", "");
												if (ces.getPolygonOKS().size() != 0) {
													List<Polygon> gbl = ces.getPolygonOKS();
													for (int h=0; h < gbl.size(); h++) {
														try {
															if (gbl.get(h) != null) {
																featureBuilderRealty.add(gbl.get(h));
																featureBuilderRealty.add("Сооружение");
																featureBuilderRealty.add(getPurpose(under.getObjectUnderConstructionRecord().get(j)));
																featureBuilderRealty.add(getCadNum(under.getObjectUnderConstructionRecord().get(j)));
																cn = getCadNum(under.getObjectUnderConstructionRecord().get(j));
																featureBuilderRealty.add(LandProcessorConstants.NODATA);
																featureBuilderRealty.add(LandProcessorConstants.NODATA);
																featureBuilderRealty.add(0.0);
																featureBuilderRealty.add(getAdr1(under.getObjectUnderConstructionRecord().get(j)));
																featureBuilderRealty.add(getStreet(under.getObjectUnderConstructionRecord().get(j)));
																featureBuilderRealty.add(getLeve1(under.getObjectUnderConstructionRecord().get(j)));
																featureBuilderRealty.add(getLeve2(under.getObjectUnderConstructionRecord().get(j)));
																featureBuilderRealty.add(getLeve3(under.getObjectUnderConstructionRecord().get(j)));
																featureBuilderRealty.add(getAppart(under.getObjectUnderConstructionRecord().get(j)));
																featureBuilderRealty.add(getOther(under.getObjectUnderConstructionRecord().get(j)));
																featureBuilderRealty.add(getNote(under.getObjectUnderConstructionRecord().get(j)));
																SimpleFeature buildP = featureBuilderRealty.buildFeature(null);
																realtyCollection.add(buildP);
															}
														} catch (Exception ex) {
															System.err.println("Error in create under constructions; " + cn + ";" + ex.getMessage());
														}
													}
												}
												if (ces.getLineOKS().size() != 0) {
													List<LineString> gbl = ces.getLineOKS();
													cn = "?";
													for (int h=0; h < gbl.size(); h++) {
														try {
															if (gbl.get(h) != null) {
																featureBuilderRealtyLine.add(gbl.get(h));
																featureBuilderRealtyLine.add("Сооружение линейное");
																featureBuilderRealtyLine.add(getPurpose(under.getObjectUnderConstructionRecord().get(j)));
																featureBuilderRealtyLine.add(getCadNum(under.getObjectUnderConstructionRecord().get(j)));
                                                                cn = getCadNum(under.getObjectUnderConstructionRecord().get(j));
																featureBuilderRealtyLine.add(LandProcessorConstants.NODATA);
																featureBuilderRealtyLine.add(LandProcessorConstants.NODATA);
																featureBuilderRealtyLine.add(0.0);
																featureBuilderRealtyLine.add(getAdr1(under.getObjectUnderConstructionRecord().get(j)));
																featureBuilderRealtyLine.add(getStreet(under.getObjectUnderConstructionRecord().get(j)));
																featureBuilderRealtyLine.add(getLeve1(under.getObjectUnderConstructionRecord().get(j)));
																featureBuilderRealtyLine.add(getLeve2(under.getObjectUnderConstructionRecord().get(j)));
																featureBuilderRealtyLine.add(getLeve3(under.getObjectUnderConstructionRecord().get(j)));
																featureBuilderRealtyLine.add(getAppart(under.getObjectUnderConstructionRecord().get(j)));
																featureBuilderRealtyLine.add(getOther(under.getObjectUnderConstructionRecord().get(j)));
																featureBuilderRealtyLine.add(getNote(under.getObjectUnderConstructionRecord().get(j)));
																SimpleFeature buildL = featureBuilderRealtyLine.buildFeature(null);
																realtyLineCollection.add(buildL);
															}
														} catch (Exception ex) {
															System.err.println("Error in create under constructions Line; " + cn + ";" + ex.getMessage());
														}

													}
												}
											}
										}

									}
								}
								System.out.println("Finish realty...");
							}
						}

					}
				}
			}

		} catch (JAXBException ex) {
			System.err.println("JAXBException " + ex.getMessage());
		} catch (Exception ex){
            ex.printStackTrace();
			System.err.println(ex.getMessage());
		}

	}

	private String getPermittedUses(BaseDataType.BuildRecords.BuildRecord Build) {
		try {
			if (Build.getParams().getPermittedUses() != null){
				String val = "";
				for (int i = 0; i < Build.getParams().getPermittedUses().getPermittedUse().size(); i++) {
					val += Build.getParams().getPermittedUses().getPermittedUse().get(i).getName()+";";
				}
				return val;
			} else {
				return LandProcessorConstants.NODATA;
			}
		} catch (Exception ex) {
			return LandProcessorConstants.NODATA;
		}
	}
	private String getPermittedUses(BaseDataType.ConstructionRecords.ConstructionRecord Build) {
		try {
			if (Build.getParams().getPermittedUses() != null){
				String val = "";
				for (int i = 0; i < Build.getParams().getPermittedUses().getPermittedUse().size(); i++) {
					val += Build.getParams().getPermittedUses().getPermittedUse().get(i).getName()+";";
				}
				return val;
			} else {
				return LandProcessorConstants.NODATA;
			}
		} catch (Exception ex) {
			return LandProcessorConstants.NODATA;
		}
	}

	private String getPurpose(BaseDataType.BuildRecords.BuildRecord Build) {
		try {
			if (Build.getParams().getPurpose() != null){
				return Build.getParams().getPurpose().getValue();
			} else {
				return LandProcessorConstants.NODATA;
			}
		} catch (Exception ex) {
			return LandProcessorConstants.NODATA;
		}
	}
	private String getPurpose(BaseDataType.ConstructionRecords.ConstructionRecord Build) {
		try {
			if (Build.getParams().getPurpose() != null){
				return Build.getParams().getPurpose();
			} else {
				return LandProcessorConstants.NODATA;
			}
		} catch (Exception ex) {
			return LandProcessorConstants.NODATA;
		}
	}
	private String getPurpose(BaseDataType.ObjectUnderConstructionRecords.ObjectUnderConstructionRecord Build) {
		try {
			if (Build.getParams().getPurpose() != null){
				return Build.getParams().getPurpose();
			} else {
				return LandProcessorConstants.NODATA;
			}
		} catch (Exception ex) {
			return LandProcessorConstants.NODATA;
		}
	}

	// Parcels prop
	// Установленное разрешенное использование
	private String getCategory (BaseDataType.LandRecords.LandRecord Land) {
		try {
			if (Land.getParams().getCategory() != null){
				return Land.getParams().getCategory().getType().getValue();
			} else {
				return LandProcessorConstants.NODATA;
			}
		} catch (Exception ex) {
			return LandProcessorConstants.NODATA;
		}
	}
	// Установленное разрешенное использование
	private String getPermUseByDoc (BaseDataType.LandRecords.LandRecord Land) {
		try {
			if (Land.getParams().getPermittedUse() != null && Land.getParams().getPermittedUse().getPermittedUseEstablished() != null){
				return Land.getParams().getPermittedUse().getPermittedUseEstablished().getByDocument();
			} else {
				return LandProcessorConstants.NODATA;
			}
		} catch (Exception ex) {
			return LandProcessorConstants.NODATA;
		}
	}
	// Установленное разрешенное использование
	private String getPermUseByType (BaseDataType.LandRecords.LandRecord Land) {
		try {
			if (Land.getParams().getPermittedUse() != null && Land.getParams().getPermittedUse().getPermittedUseEstablished() != null){
				return Land.getParams().getPermittedUse().getPermittedUseEstablished().getLandUse().getValue();
			} else {
				return LandProcessorConstants.NODATA;
			}
		} catch (Exception ex) {
			return LandProcessorConstants.NODATA;
		}
	}
	// Разрешенное использование по градостроительному регламенту
	private String getPermUseByGradReg (BaseDataType.LandRecords.LandRecord Land) {
		try {
			if (Land.getParams().getPermittesUsesGradReg() != null){
				return Land.getParams().getPermittesUsesGradReg().getPermittedUseText();
			} else {
				return LandProcessorConstants.NODATA;
			}
		} catch (Exception ex) {
			return LandProcessorConstants.NODATA;
		}
	}
    private double getAreaInaccur (BaseDataType.LandRecords.LandRecord Land) {
        try {
            if (Land.getParams().getArea() != null && Land.getParams().getArea().getInaccuracy() != null){
                return Land.getParams().getArea().getInaccuracy().doubleValue();
            } else {
                return -1.0;
            }
        } catch (Exception ex) {
            return -1.0;
        }
    }
	private double getArea (BaseDataType.LandRecords.LandRecord Land) {
		try {
			if (Land.getParams().getArea() != null){
				return Land.getParams().getArea().getValue().doubleValue();
			} else {
				return -1.0;
			}
		} catch (Exception ex) {
			return -1.0;
		}
	}
	private double getArea (BaseDataType.BuildRecords.BuildRecord Land) {
		try {
			if (Land.getParams().getArea() != null){
				return Land.getParams().getArea().doubleValue();
			} else {
				return -1.0;
			}
		} catch (Exception ex) {
			return -1.0;
		}
	}

	// Общие сведения о земельном участке (вид)
	private String getSubType (BaseDataType.LandRecords.LandRecord Land) {
		try {
			if (Land.getObject().getSubtype() != null){
				return Land.getObject().getSubtype().getValue();
			} else {
				return LandProcessorConstants.NODATA;
			}
		} catch (Exception ex) {
			return LandProcessorConstants.NODATA;
		}
	}
	// Общие сведения о земельном участке (кадастровый номер)
	private String getCadNum (BaseDataType.LandRecords.LandRecord Land) {
		try {
			if (Land.getObject().getCommonData() != null && Land.getObject().getCommonData().getCadNumber() != null){
				return Land.getObject().getCommonData().getCadNumber();
			} else {
				return LandProcessorConstants.NODATA;
			}
		} catch (Exception ex) {
			return LandProcessorConstants.NODATA;
		}
	}
	private String getCadNum (BaseDataType.BuildRecords.BuildRecord Build) {
		try {
			if (Build.getObject().getCommonData() != null && Build.getObject().getCommonData().getCadNumber() != null){
				return Build.getObject().getCommonData().getCadNumber();
			} else {
				return LandProcessorConstants.NODATA;
			}
		} catch (Exception ex) {
			return LandProcessorConstants.NODATA;
		}
	}
	private String getCadNum (BaseDataType.ConstructionRecords.ConstructionRecord Build) {
		try {
			if (Build.getObject().getCommonData() != null && Build.getObject().getCommonData().getCadNumber() != null){
				return Build.getObject().getCommonData().getCadNumber();
			} else {
				return LandProcessorConstants.NODATA;
			}
		} catch (Exception ex) {
			return LandProcessorConstants.NODATA;
		}
	}
	private String getCadNum (BaseDataType.ObjectUnderConstructionRecords.ObjectUnderConstructionRecord Build) {
		try {
			if (Build.getObject().getCommonData() != null && Build.getObject().getCommonData().getCadNumber() != null){
				return Build.getObject().getCommonData().getCadNumber();
			} else {
				return LandProcessorConstants.NODATA;
			}
		} catch (Exception ex) {
			return LandProcessorConstants.NODATA;
		}
	}
	private String getCommonCadNum (BaseDataType.LandRecords.LandRecord Land) {
		try {
			if (Land.getCadLinks().getCommonLand() != null && (Land.getCadLinks().getCommonLand().getCommonLandCadNumber().getCadNumber() != null)){
				return Land.getCadLinks().getCommonLand().getCommonLandCadNumber().getCadNumber();
			} else {
				return LandProcessorConstants.NODATA;
			}
		} catch (Exception ex) {
			return LandProcessorConstants.NODATA;
		}
	}
	// Общие сведения о земельном участке (кадастровый номер)
	private String getType (BaseDataType.LandRecords.LandRecord Land) {
		try {
			if (Land.getObject().getCommonData() != null && Land.getObject().getCommonData().getType() != null){
				return Land.getObject().getCommonData().getType().getValue();
			} else {
				return LandProcessorConstants.NODATA;
			}
		} catch (Exception ex) {
			return LandProcessorConstants.NODATA;
		}
	}

	private String getType (BaseDataType.BuildRecords.BuildRecord Build) {
		try {
			if (Build.getObject().getCommonData() != null && Build.getObject().getCommonData().getType() != null){
				return Build.getObject().getCommonData().getType().getValue();
			} else {
				return LandProcessorConstants.NODATA;
			}
		} catch (Exception ex) {
			return LandProcessorConstants.NODATA;
		}
	}
	private String getType (BaseDataType.ConstructionRecords.ConstructionRecord Build) {
		try {
			if (Build.getObject().getCommonData() != null && Build.getObject().getCommonData().getType() != null){
				return Build.getObject().getCommonData().getType().getValue();
			} else {
				return LandProcessorConstants.NODATA;
			}
		} catch (Exception ex) {
			return LandProcessorConstants.NODATA;
		}
	}
	private String getType (BaseDataType.ObjectUnderConstructionRecords.ObjectUnderConstructionRecord Build) {
		try {
			if (Build.getObject().getCommonData() != null && Build.getObject().getCommonData().getType() != null){
				return Build.getObject().getCommonData().getType().getValue();
			} else {
				return LandProcessorConstants.NODATA;
			}
		} catch (Exception ex) {
			return LandProcessorConstants.NODATA;
		}
	}
	//
	private String getAdr1 (BaseDataType.LandRecords.LandRecord Land) {
		try {
			if (Land.getAddressLocation().getAddress() == null || Land.getAddressLocation().getAddress().getAddressFias() == null){
				return LandProcessorConstants.NODATA;
			}
			String region = Land.getAddressLocation().getAddress().getAddressFias().getLevelSettlement().getRegion().getValue();
			String district = "";
			String city = "";
			String urbanDistrict = "";
			String selsovet = "";
			if (Land.getAddressLocation().getAddress().getAddressFias().getLevelSettlement().getDistrict() != null){
				district = Land.getAddressLocation().getAddress().getAddressFias().getLevelSettlement().getDistrict().getTypeDistrict() +
					" " +Land.getAddressLocation().getAddress().getAddressFias().getLevelSettlement().getDistrict().getNameDistrict();
			}
			if (Land.getAddressLocation().getAddress().getAddressFias().getLevelSettlement().getCity() != null){
				city = Land.getAddressLocation().getAddress().getAddressFias().getLevelSettlement().getCity().getTypeCity() +
					" "+ Land.getAddressLocation().getAddress().getAddressFias().getLevelSettlement().getCity().getNameCity();
			}
			if (Land.getAddressLocation().getAddress().getAddressFias().getLevelSettlement().getUrbanDistrict() != null){
				urbanDistrict = Land.getAddressLocation().getAddress().getAddressFias().getLevelSettlement().getUrbanDistrict().getTypeUrbanDistrict()+
					" "+Land.getAddressLocation().getAddress().getAddressFias().getLevelSettlement().getUrbanDistrict().getNameUrbanDistrict();
			}
			if (Land.getAddressLocation().getAddress().getAddressFias().getLevelSettlement().getSovietVillage() != null){
				selsovet = Land.getAddressLocation().getAddress().getAddressFias().getLevelSettlement().getSovietVillage().getTypeSovietVillage()+
					" "+Land.getAddressLocation().getAddress().getAddressFias().getLevelSettlement().getSovietVillage().getNameSovietVillage();
			}

			return region + ","+district+","+city+","+urbanDistrict+","+selsovet;
		} catch (Exception ex) {
			return LandProcessorConstants.NODATA;
		}
	}
	private String getAdr1 (BaseDataType.BuildRecords.BuildRecord Land) {
		try {
			if (Land.getAddressLocation().getAddress() == null || Land.getAddressLocation().getAddress().getAddressFias() == null){
				return LandProcessorConstants.NODATA;
			}
			String region = Land.getAddressLocation().getAddress().getAddressFias().getLevelSettlement().getRegion().getValue();
			String district = "";
			String city = "";
			String urbanDistrict = "";
			String selsovet = "";
			if (Land.getAddressLocation().getAddress().getAddressFias().getLevelSettlement().getDistrict() != null){
				district = Land.getAddressLocation().getAddress().getAddressFias().getLevelSettlement().getDistrict().getTypeDistrict() +
						" " +Land.getAddressLocation().getAddress().getAddressFias().getLevelSettlement().getDistrict().getNameDistrict();
			}
			if (Land.getAddressLocation().getAddress().getAddressFias().getLevelSettlement().getCity() != null){
				city = Land.getAddressLocation().getAddress().getAddressFias().getLevelSettlement().getCity().getTypeCity() +
						" "+ Land.getAddressLocation().getAddress().getAddressFias().getLevelSettlement().getCity().getNameCity();
			}
			if (Land.getAddressLocation().getAddress().getAddressFias().getLevelSettlement().getUrbanDistrict() != null){
				urbanDistrict = Land.getAddressLocation().getAddress().getAddressFias().getLevelSettlement().getUrbanDistrict().getTypeUrbanDistrict()+
						" "+Land.getAddressLocation().getAddress().getAddressFias().getLevelSettlement().getUrbanDistrict().getNameUrbanDistrict();
			}
			if (Land.getAddressLocation().getAddress().getAddressFias().getLevelSettlement().getSovietVillage() != null){
				selsovet = Land.getAddressLocation().getAddress().getAddressFias().getLevelSettlement().getSovietVillage().getTypeSovietVillage()+
						" "+Land.getAddressLocation().getAddress().getAddressFias().getLevelSettlement().getSovietVillage().getNameSovietVillage();
			}

			return region + ","+district+","+city+","+urbanDistrict+","+selsovet;
		} catch (Exception ex) {
			return LandProcessorConstants.NODATA;
		}
	}
	private String getAdr1 (BaseDataType.ConstructionRecords.ConstructionRecord Land) {
		try {
			if (Land.getAddressLocation().getAddress() == null || Land.getAddressLocation().getAddress().getAddressFias() == null){
				return LandProcessorConstants.NODATA;
			}
			String region = Land.getAddressLocation().getAddress().getAddressFias().getLevelSettlement().getRegion().getValue();
			String district = "";
			String city = "";
			String urbanDistrict = "";
			String selsovet = "";
			if (Land.getAddressLocation().getAddress().getAddressFias().getLevelSettlement().getDistrict() != null){
				district = Land.getAddressLocation().getAddress().getAddressFias().getLevelSettlement().getDistrict().getTypeDistrict() +
						" " +Land.getAddressLocation().getAddress().getAddressFias().getLevelSettlement().getDistrict().getNameDistrict();
			}
			if (Land.getAddressLocation().getAddress().getAddressFias().getLevelSettlement().getCity() != null){
				city = Land.getAddressLocation().getAddress().getAddressFias().getLevelSettlement().getCity().getTypeCity() +
						" "+ Land.getAddressLocation().getAddress().getAddressFias().getLevelSettlement().getCity().getNameCity();
			}
			if (Land.getAddressLocation().getAddress().getAddressFias().getLevelSettlement().getUrbanDistrict() != null){
				urbanDistrict = Land.getAddressLocation().getAddress().getAddressFias().getLevelSettlement().getUrbanDistrict().getTypeUrbanDistrict()+
						" "+Land.getAddressLocation().getAddress().getAddressFias().getLevelSettlement().getUrbanDistrict().getNameUrbanDistrict();
			}
			if (Land.getAddressLocation().getAddress().getAddressFias().getLevelSettlement().getSovietVillage() != null){
				selsovet = Land.getAddressLocation().getAddress().getAddressFias().getLevelSettlement().getSovietVillage().getTypeSovietVillage()+
						" "+Land.getAddressLocation().getAddress().getAddressFias().getLevelSettlement().getSovietVillage().getNameSovietVillage();
			}

			return region + ","+district+","+city+","+urbanDistrict+","+selsovet;
		} catch (Exception ex) {
			return LandProcessorConstants.NODATA;
		}
	}
	private String getAdr1 (BaseDataType.ObjectUnderConstructionRecords.ObjectUnderConstructionRecord Land) {
		try {
			if (Land.getAddressLocation().getAddress() == null || Land.getAddressLocation().getAddress().getAddressFias() == null){
				return LandProcessorConstants.NODATA;
			}
			String region = Land.getAddressLocation().getAddress().getAddressFias().getLevelSettlement().getRegion().getValue();
			String district = "";
			String city = "";
			String urbanDistrict = "";
			String selsovet = "";
			if (Land.getAddressLocation().getAddress().getAddressFias().getLevelSettlement().getDistrict() != null){
				district = Land.getAddressLocation().getAddress().getAddressFias().getLevelSettlement().getDistrict().getTypeDistrict() +
						" " +Land.getAddressLocation().getAddress().getAddressFias().getLevelSettlement().getDistrict().getNameDistrict();
			}
			if (Land.getAddressLocation().getAddress().getAddressFias().getLevelSettlement().getCity() != null){
				city = Land.getAddressLocation().getAddress().getAddressFias().getLevelSettlement().getCity().getTypeCity() +
						" "+ Land.getAddressLocation().getAddress().getAddressFias().getLevelSettlement().getCity().getNameCity();
			}
			if (Land.getAddressLocation().getAddress().getAddressFias().getLevelSettlement().getUrbanDistrict() != null){
				urbanDistrict = Land.getAddressLocation().getAddress().getAddressFias().getLevelSettlement().getUrbanDistrict().getTypeUrbanDistrict()+
						" "+Land.getAddressLocation().getAddress().getAddressFias().getLevelSettlement().getUrbanDistrict().getNameUrbanDistrict();
			}
			if (Land.getAddressLocation().getAddress().getAddressFias().getLevelSettlement().getSovietVillage() != null){
				selsovet = Land.getAddressLocation().getAddress().getAddressFias().getLevelSettlement().getSovietVillage().getTypeSovietVillage()+
						" "+Land.getAddressLocation().getAddress().getAddressFias().getLevelSettlement().getSovietVillage().getNameSovietVillage();
			}

			return region + ","+district+","+city+","+urbanDistrict+","+selsovet;
		} catch (Exception ex) {
			return LandProcessorConstants.NODATA;
		}
	}

	private String getStreet (BaseDataType.LandRecords.LandRecord Land) {
		try {
			if (Land.getAddressLocation().getAddress().getAddressFias() != null && Land.getAddressLocation().getAddress().getAddressFias().getDetailedLevel().getStreet() != null){
				return  Land.getAddressLocation().getAddress().getAddressFias().getDetailedLevel().getStreet().getTypeStreet()+" "+
					Land.getAddressLocation().getAddress().getAddressFias().getDetailedLevel().getStreet().getNameStreet();
			} else {
				return LandProcessorConstants.NODATA;
			}
		} catch (Exception ex) {
			return LandProcessorConstants.NODATA;
		}
	}
	private String getStreet (BaseDataType.BuildRecords.BuildRecord Land) {
		try {
			if (Land.getAddressLocation().getAddress().getAddressFias() != null && Land.getAddressLocation().getAddress().getAddressFias().getDetailedLevel().getStreet() != null){
				return  Land.getAddressLocation().getAddress().getAddressFias().getDetailedLevel().getStreet().getTypeStreet()+""+
						Land.getAddressLocation().getAddress().getAddressFias().getDetailedLevel().getStreet().getNameStreet();
			} else {
				return LandProcessorConstants.NODATA;
			}
		} catch (Exception ex) {
			return LandProcessorConstants.NODATA;
		}
	}
	private String getStreet (BaseDataType.ConstructionRecords.ConstructionRecord Land) {
		try {
			if (Land.getAddressLocation().getAddress().getAddressFias() != null && Land.getAddressLocation().getAddress().getAddressFias().getDetailedLevel().getStreet() != null){
				return  Land.getAddressLocation().getAddress().getAddressFias().getDetailedLevel().getStreet().getTypeStreet()+""+
						Land.getAddressLocation().getAddress().getAddressFias().getDetailedLevel().getStreet().getNameStreet();
			} else {
				return LandProcessorConstants.NODATA;
			}
		} catch (Exception ex) {
			return LandProcessorConstants.NODATA;
		}
	}
	private String getStreet (BaseDataType.ObjectUnderConstructionRecords.ObjectUnderConstructionRecord Land) {
		try {
			if (Land.getAddressLocation().getAddress().getAddressFias() != null && Land.getAddressLocation().getAddress().getAddressFias().getDetailedLevel().getStreet() != null){
				return  Land.getAddressLocation().getAddress().getAddressFias().getDetailedLevel().getStreet().getTypeStreet()+""+
						Land.getAddressLocation().getAddress().getAddressFias().getDetailedLevel().getStreet().getNameStreet();
			} else {
				return LandProcessorConstants.NODATA;
			}
		} catch (Exception ex) {
			return LandProcessorConstants.NODATA;
		}
	}
	private String getLeve1 (BaseDataType.LandRecords.LandRecord Land) {
		try {
			if (Land.getAddressLocation().getAddress().getAddressFias() != null && Land.getAddressLocation().getAddress().getAddressFias().getDetailedLevel().getLevel1() != null){
				return  Land.getAddressLocation().getAddress().getAddressFias().getDetailedLevel().getLevel1().getTypeLevel1()+" "+
						Land.getAddressLocation().getAddress().getAddressFias().getDetailedLevel().getLevel1().getNameLevel1();
			} else {
				return LandProcessorConstants.NODATA;
			}
		} catch (Exception ex) {
			return LandProcessorConstants.NODATA;
		}
	}
	private String getLeve1 (BaseDataType.BuildRecords.BuildRecord Land) {
		try {
			if (Land.getAddressLocation().getAddress().getAddressFias() != null && Land.getAddressLocation().getAddress().getAddressFias().getDetailedLevel().getLevel1() != null){
				return  Land.getAddressLocation().getAddress().getAddressFias().getDetailedLevel().getLevel1().getTypeLevel1()+" "+
						Land.getAddressLocation().getAddress().getAddressFias().getDetailedLevel().getLevel1().getNameLevel1();
			} else {
				return LandProcessorConstants.NODATA;
			}
		} catch (Exception ex) {
			return LandProcessorConstants.NODATA;
		}
	}
	private String getLeve1 (BaseDataType.ConstructionRecords.ConstructionRecord Land) {
		try {
			if (Land.getAddressLocation().getAddress().getAddressFias() != null && Land.getAddressLocation().getAddress().getAddressFias().getDetailedLevel().getLevel1() != null){
				return  Land.getAddressLocation().getAddress().getAddressFias().getDetailedLevel().getLevel1().getTypeLevel1()+" "+
						Land.getAddressLocation().getAddress().getAddressFias().getDetailedLevel().getLevel1().getNameLevel1();
			} else {
				return LandProcessorConstants.NODATA;
			}
		} catch (Exception ex) {
			return LandProcessorConstants.NODATA;
		}
	}
	private String getLeve1 (BaseDataType.ObjectUnderConstructionRecords.ObjectUnderConstructionRecord Land) {
		try {
			if (Land.getAddressLocation().getAddress().getAddressFias() != null && Land.getAddressLocation().getAddress().getAddressFias().getDetailedLevel().getLevel1() != null){
				return  Land.getAddressLocation().getAddress().getAddressFias().getDetailedLevel().getLevel1().getTypeLevel1()+" "+
						Land.getAddressLocation().getAddress().getAddressFias().getDetailedLevel().getLevel1().getNameLevel1();
			} else {
				return LandProcessorConstants.NODATA;
			}
		} catch (Exception ex) {
			return LandProcessorConstants.NODATA;
		}
	}
	private String getLeve2 (BaseDataType.LandRecords.LandRecord Land) {
		try {
			if (Land.getAddressLocation().getAddress().getAddressFias() != null && Land.getAddressLocation().getAddress().getAddressFias().getDetailedLevel().getLevel2() != null){
				return  Land.getAddressLocation().getAddress().getAddressFias().getDetailedLevel().getLevel2().getTypeLevel2()+" "+
					Land.getAddressLocation().getAddress().getAddressFias().getDetailedLevel().getLevel2().getNameLevel2();
			} else {
				return LandProcessorConstants.NODATA;
			}
		} catch (Exception ex) {
			return LandProcessorConstants.NODATA;
		}
	}
	private String getLeve2 (BaseDataType.BuildRecords.BuildRecord Land) {
		try {
			if (Land.getAddressLocation().getAddress().getAddressFias() != null && Land.getAddressLocation().getAddress().getAddressFias().getDetailedLevel().getLevel2() != null){
				return  Land.getAddressLocation().getAddress().getAddressFias().getDetailedLevel().getLevel2().getTypeLevel2()+" "+
						Land.getAddressLocation().getAddress().getAddressFias().getDetailedLevel().getLevel2().getNameLevel2();
			} else {
				return LandProcessorConstants.NODATA;
			}
		} catch (Exception ex) {
			return LandProcessorConstants.NODATA;
		}
	}
	private String getLeve2 (BaseDataType.ConstructionRecords.ConstructionRecord Land) {
		try {
			if (Land.getAddressLocation().getAddress().getAddressFias() != null && Land.getAddressLocation().getAddress().getAddressFias().getDetailedLevel().getLevel2() != null){
				return  Land.getAddressLocation().getAddress().getAddressFias().getDetailedLevel().getLevel2().getTypeLevel2()+" "+
						Land.getAddressLocation().getAddress().getAddressFias().getDetailedLevel().getLevel2().getNameLevel2();
			} else {
				return LandProcessorConstants.NODATA;
			}
		} catch (Exception ex) {
			return LandProcessorConstants.NODATA;
		}
	}
	private String getLeve2 (BaseDataType.ObjectUnderConstructionRecords.ObjectUnderConstructionRecord Land) {
		try {
			if (Land.getAddressLocation().getAddress().getAddressFias() != null && Land.getAddressLocation().getAddress().getAddressFias().getDetailedLevel().getLevel2() != null){
				return  Land.getAddressLocation().getAddress().getAddressFias().getDetailedLevel().getLevel2().getTypeLevel2()+" "+
						Land.getAddressLocation().getAddress().getAddressFias().getDetailedLevel().getLevel2().getNameLevel2();
			} else {
				return LandProcessorConstants.NODATA;
			}
		} catch (Exception ex) {
			return LandProcessorConstants.NODATA;
		}
	}
	private String getLeve3 (BaseDataType.LandRecords.LandRecord Land) {
		try {
			if (Land.getAddressLocation().getAddress().getAddressFias() != null && Land.getAddressLocation().getAddress().getAddressFias().getDetailedLevel().getLevel3() != null){
				return  Land.getAddressLocation().getAddress().getAddressFias().getDetailedLevel().getLevel3().getTypeLevel3()+" "+
					Land.getAddressLocation().getAddress().getAddressFias().getDetailedLevel().getLevel3().getNameLevel3();
			} else {
				return LandProcessorConstants.NODATA;
			}
		} catch (Exception ex) {
			return LandProcessorConstants.NODATA;
		}
	}
	private String getLeve3 (BaseDataType.BuildRecords.BuildRecord Land) {
		try {
			if (Land.getAddressLocation().getAddress().getAddressFias() != null && Land.getAddressLocation().getAddress().getAddressFias().getDetailedLevel().getLevel3() != null){
				return  Land.getAddressLocation().getAddress().getAddressFias().getDetailedLevel().getLevel3().getTypeLevel3()+" "+
						Land.getAddressLocation().getAddress().getAddressFias().getDetailedLevel().getLevel3().getNameLevel3();
			} else {
				return LandProcessorConstants.NODATA;
			}
		} catch (Exception ex) {
			return LandProcessorConstants.NODATA;
		}
	}
	private String getLeve3 (BaseDataType.ConstructionRecords.ConstructionRecord Land) {
		try {
			if (Land.getAddressLocation().getAddress().getAddressFias() != null && Land.getAddressLocation().getAddress().getAddressFias().getDetailedLevel().getLevel3() != null){
				return  Land.getAddressLocation().getAddress().getAddressFias().getDetailedLevel().getLevel3().getTypeLevel3()+" "+
						Land.getAddressLocation().getAddress().getAddressFias().getDetailedLevel().getLevel3().getNameLevel3();
			} else {
				return LandProcessorConstants.NODATA;
			}
		} catch (Exception ex) {
			return LandProcessorConstants.NODATA;
		}
	}
	private String getLeve3 (BaseDataType.ObjectUnderConstructionRecords.ObjectUnderConstructionRecord Land) {
		try {
			if (Land.getAddressLocation().getAddress().getAddressFias() != null && Land.getAddressLocation().getAddress().getAddressFias().getDetailedLevel().getLevel3() != null){
				return  Land.getAddressLocation().getAddress().getAddressFias().getDetailedLevel().getLevel3().getTypeLevel3()+" "+
						Land.getAddressLocation().getAddress().getAddressFias().getDetailedLevel().getLevel3().getNameLevel3();
			} else {
				return LandProcessorConstants.NODATA;
			}
		} catch (Exception ex) {
			return LandProcessorConstants.NODATA;
		}
	}
	private String getAppart (BaseDataType.LandRecords.LandRecord Land) {
		try {
			if (Land.getAddressLocation().getAddress().getAddressFias() != null && Land.getAddressLocation().getAddress().getAddressFias().getDetailedLevel().getApartment() != null){
				return  Land.getAddressLocation().getAddress().getAddressFias().getDetailedLevel().getApartment().getTypeApartment()+" "+
					Land.getAddressLocation().getAddress().getAddressFias().getDetailedLevel().getApartment().getNameApartment();
			} else {
				return LandProcessorConstants.NODATA;
			}
		} catch (Exception ex) {
			return LandProcessorConstants.NODATA;
		}
	}
	private String getAppart (BaseDataType.BuildRecords.BuildRecord Land) {
		try {
			if (Land.getAddressLocation().getAddress().getAddressFias() != null && Land.getAddressLocation().getAddress().getAddressFias().getDetailedLevel().getApartment() != null){
				return  Land.getAddressLocation().getAddress().getAddressFias().getDetailedLevel().getApartment().getTypeApartment()+" "+
						Land.getAddressLocation().getAddress().getAddressFias().getDetailedLevel().getApartment().getNameApartment();
			} else {
				return LandProcessorConstants.NODATA;
			}
		} catch (Exception ex) {
			return LandProcessorConstants.NODATA;
		}
	}
	private String getAppart (BaseDataType.ConstructionRecords.ConstructionRecord Land) {
		try {
			if (Land.getAddressLocation().getAddress().getAddressFias() != null && Land.getAddressLocation().getAddress().getAddressFias().getDetailedLevel().getApartment() != null){
				return  Land.getAddressLocation().getAddress().getAddressFias().getDetailedLevel().getApartment().getTypeApartment()+" "+
						Land.getAddressLocation().getAddress().getAddressFias().getDetailedLevel().getApartment().getNameApartment();
			} else {
				return LandProcessorConstants.NODATA;
			}
		} catch (Exception ex) {
			return LandProcessorConstants.NODATA;
		}
	}
	private String getAppart (BaseDataType.ObjectUnderConstructionRecords.ObjectUnderConstructionRecord Land) {
		try {
			if (Land.getAddressLocation().getAddress().getAddressFias() != null && Land.getAddressLocation().getAddress().getAddressFias().getDetailedLevel().getApartment() != null){
				return  Land.getAddressLocation().getAddress().getAddressFias().getDetailedLevel().getApartment().getTypeApartment()+" "+
						Land.getAddressLocation().getAddress().getAddressFias().getDetailedLevel().getApartment().getNameApartment();
			} else {
				return LandProcessorConstants.NODATA;
			}
		} catch (Exception ex) {
			return LandProcessorConstants.NODATA;
		}
	}
	private String getAdrType (BaseDataType.LandRecords.LandRecord Land) {
		try {
			if (Land.getAddressLocation().getAddressType() != null){
				return  Land.getAddressLocation().getAddressType().getValue();
			} else {
				return LandProcessorConstants.NODATA;
			}
		} catch (Exception ex) {
			return LandProcessorConstants.NODATA;
		}
	}
	private String getAdrType (BaseDataType.BuildRecords.BuildRecord Land) {
		try {
			if (Land.getAddressLocation().getAddressType() != null){
				return  Land.getAddressLocation().getAddressType().getValue();
			} else {
				return LandProcessorConstants.NODATA;
			}
		} catch (Exception ex) {
			return LandProcessorConstants.NODATA;
		}
	}
	private String getAdrType (BaseDataType.ConstructionRecords.ConstructionRecord Land) {
		try {
			if (Land.getAddressLocation().getAddressType() != null){
				return  Land.getAddressLocation().getAddressType().getValue();
			} else {
				return LandProcessorConstants.NODATA;
			}
		} catch (Exception ex) {
			return LandProcessorConstants.NODATA;
		}
	}
	private String getAdrType (BaseDataType.ObjectUnderConstructionRecords.ObjectUnderConstructionRecord Land) {
		try {
			if (Land.getAddressLocation().getAddressType() != null){
				return  Land.getAddressLocation().getAddressType().getValue();
			} else {
				return LandProcessorConstants.NODATA;
			}
		} catch (Exception ex) {
			return LandProcessorConstants.NODATA;
		}
	}
	private String getOther (BaseDataType.LandRecords.LandRecord Land) {
		try {
			if (Land.getAddressLocation().getAddress().getAddressFias() != null && Land.getAddressLocation().getAddress().getAddressFias().getDetailedLevel().getOther() != null){
				return  Land.getAddressLocation().getAddress().getAddressFias().getDetailedLevel().getOther();
			} else {
				return LandProcessorConstants.NODATA;
			}
		} catch (Exception ex) {
			return LandProcessorConstants.NODATA;
		}
	}
	private String getOther (BaseDataType.BuildRecords.BuildRecord Land) {
		try {
			if (Land.getAddressLocation().getAddress().getAddressFias() != null && Land.getAddressLocation().getAddress().getAddressFias().getDetailedLevel().getOther() != null){
				return  Land.getAddressLocation().getAddress().getAddressFias().getDetailedLevel().getOther();
			} else {
				return LandProcessorConstants.NODATA;
			}
		} catch (Exception ex) {
			return LandProcessorConstants.NODATA;
		}
	}
	private String getOther (BaseDataType.ConstructionRecords.ConstructionRecord Land) {
		try {
			if (Land.getAddressLocation().getAddress().getAddressFias() != null && Land.getAddressLocation().getAddress().getAddressFias().getDetailedLevel().getOther() != null){
				return  Land.getAddressLocation().getAddress().getAddressFias().getDetailedLevel().getOther();
			} else {
				return LandProcessorConstants.NODATA;
			}
		} catch (Exception ex) {
			return LandProcessorConstants.NODATA;
		}
	}
	private String getOther (BaseDataType.ObjectUnderConstructionRecords.ObjectUnderConstructionRecord Land) {
		try {
			if (Land.getAddressLocation().getAddress().getAddressFias() != null && Land.getAddressLocation().getAddress().getAddressFias().getDetailedLevel().getOther() != null){
				return  Land.getAddressLocation().getAddress().getAddressFias().getDetailedLevel().getOther();
			} else {
				return LandProcessorConstants.NODATA;
			}
		} catch (Exception ex) {
			return LandProcessorConstants.NODATA;
		}
	}
	private String getNote (BaseDataType.LandRecords.LandRecord Land) {
		try {
			if (Land.getAddressLocation() != null && Land.getAddressLocation().getAddress().getNote() != null){
				return  Land.getAddressLocation().getAddress().getNote();
			} else {
				return LandProcessorConstants.NODATA;
			}
		} catch (Exception ex) {
			return LandProcessorConstants.NODATA;
		}
	}
	private String getNote (BaseDataType.BuildRecords.BuildRecord Land) {
		try {
			if (Land.getAddressLocation() != null && Land.getAddressLocation().getAddress().getNote() != null){
				return  Land.getAddressLocation().getAddress().getNote();
			} else {
				return LandProcessorConstants.NODATA;
			}
		} catch (Exception ex) {
			return LandProcessorConstants.NODATA;
		}
	}
	private String getNote (BaseDataType.ConstructionRecords.ConstructionRecord Land) {
		try {
			if (Land.getAddressLocation() != null && Land.getAddressLocation().getAddress().getNote() != null){
				return  Land.getAddressLocation().getAddress().getNote();
			} else {
				return LandProcessorConstants.NODATA;
			}
		} catch (Exception ex) {
			return LandProcessorConstants.NODATA;
		}
	}
	private String getNote (BaseDataType.ObjectUnderConstructionRecords.ObjectUnderConstructionRecord Land) {
		try {
			if (Land.getAddressLocation() != null && Land.getAddressLocation().getAddress().getNote() != null){
				return  Land.getAddressLocation().getAddress().getNote();
			} else {
				return LandProcessorConstants.NODATA;
			}
		} catch (Exception ex) {
			return LandProcessorConstants.NODATA;
		}
	}
	private String getLocation (BaseDataType.LandRecords.LandRecord Land) {
		try {
			String loc = "";

			if (Land.getAddressLocation() != null && Land.getAddressLocation().getRelPosition().getLocationDescription() != null){
				loc +=  Land.getAddressLocation().getRelPosition().getLocationDescription();
			}
			if (Land.getAddressLocation() != null && Land.getAddressLocation().getRelPosition().getRefPointName() != null){
				loc += " " + Land.getAddressLocation().getRelPosition().getRefPointName();
			}
			return loc;
		} catch (Exception ex) {
			return LandProcessorConstants.NODATA;
		}
	}
	private String getLocation (BaseDataType.ConstructionRecords.ConstructionRecord Land) {
		try {
			String loc = "";

			if (Land.getAddressLocation() != null && Land.getAddressLocation().getLocations() != null){
				for (int i = 0; i < Land.getAddressLocation().getLocations().getLocation().size(); i++) {
					loc += Land.getAddressLocation().getLocations().getLocation().get(i).getPositionDescription()+ ";";
				}
			}
			return loc;
		} catch (Exception ex) {
			return LandProcessorConstants.NODATA;
		}
	}
	private String getLocation (BaseDataType.ObjectUnderConstructionRecords.ObjectUnderConstructionRecord Land) {
		try {
			String loc = "";

			if (Land.getAddressLocation() != null && Land.getAddressLocation().getLocations() != null){
				for (int i = 0; i < Land.getAddressLocation().getLocations().getLocation().size(); i++) {
					loc += Land.getAddressLocation().getLocations().getLocation().get(i).getPositionDescription()+ ";";
				}
			}
			return loc;
		} catch (Exception ex) {
			return LandProcessorConstants.NODATA;
		}
	}


	// Helpers

	// Создание границ
	private void makeGeometryByESBoundList(BoundContoursOut bCntrs, SimpleFeatureBuilder featureBuilderBound, String Descr, String Type, String RegNumbBorder,String TypeBoundary, String  regDate,String quartalNumber,String respDate){
		try {
			for (int i = 0; i < bCntrs.getContour().size(); i++) {
				CadastreEntitySpatial ces = new CadastreEntitySpatial(bCntrs.getContour().get(i).getEntitySpatial());
				Polygon p = ces.getPolygon();
				if (p != null) {
					featureBuilderBound.add(p);
					featureBuilderBound.add(Type);
					featureBuilderBound.add(TypeBoundary);
					featureBuilderBound.add(RegNumbBorder);
					featureBuilderBound.add(Descr);
                    featureBuilderBound.add(regDate);
                    featureBuilderBound.add(quartalNumber);
                    featureBuilderBound.add(respDate);

					SimpleFeature bound = featureBuilderBound.buildFeature(null);
					boundCollection.add(bound);
				}
			}
		} catch (Exception ex){
			System.err.println("Error on makeGeometryByESBoundList, " + ex.getLocalizedMessage());
		}
	}
}
