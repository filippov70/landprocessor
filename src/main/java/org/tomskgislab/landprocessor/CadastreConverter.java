package org.tomskgislab.landprocessor;

import javax.xml.bind.JAXBElement;

import extract.kpt.*;


import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.File;
import java.io.FileNotFoundException;

import java.util.*;

public class CadastreConverter {
    //private static Logger logger = LogManager.getLogger(CadastreConverter.class);

    private ExtractCadastralPlanTerritory kpt = null; // КПТ


    public CadastreConverter(File PathToFile, boolean isSavePoints, String format, File saveFile) {
        try {
            Scanner scan = new Scanner(PathToFile);
            String currLine;
            while ((currLine = scan.nextLine()) != null) {
                if (currLine.contains("extract_cadastral_plan_territory") == true) {
                    parseQuartalExtract(PathToFile);
                    break;
                }
            }
        } catch (FileNotFoundException ex){
            System.err.println("File not found "+PathToFile);
        }

//        try {
//            builder = domFactory.newDocumentBuilder();
//            Document doc = builder.parse(PathToFile);
//            // КПТ
//            if (doc.getDocumentElement().getLocalName().equals("extract_cadastral_plan_territory")) {
//                doc = null;
//                builder.reset();
//                parseQuartalExtract(PathToFile);
//            } else {
//                System.out.println("This xml-file not supporting in current version LandProceesor");
//            }
//
//
//        } catch (ParserConfigurationException ex) {
//            System.err.println(ex.getMessage());
//        } catch (SAXException e) {
//            System.err.println(e.getMessage());
//        } catch (IOException e) {
//            System.err.println(e.getMessage());
//        }

    }

    private void parseQuartalExtract(File PathToFile) {
        try {
            JAXBContext context = JAXBContext.newInstance(ObjectFactory.class);
            Unmarshaller unmarshaller = context.createUnmarshaller();
            //Object obj = unmarshaller.unmarshal(PathToFile);
            JAXBElement<ExtractCadastralPlanTerritory> ecpt = (JAXBElement<ExtractCadastralPlanTerritory>)unmarshaller.unmarshal(PathToFile);
            kpt = ecpt.getValue();

            if (kpt != null) {
                System.out.println("Processing quartal " + kpt.getCadastralBlocks().getCadastralBlock().get(0).getCadastralNumber());

                if (kpt.getCadastralBlocks() != null) {
                    for (int i = 0; i < kpt.getCadastralBlocks().getCadastralBlock().size(); i++) {
                        CadastralBlock block = kpt.getCadastralBlocks().getCadastralBlock().get(i);
                        // Quartal
//                        try {
//                            quartalLyr.StartTransaction();
//                            System.out.println("Create quartal");
//                            EntitySpatialBound blockSpatial = block.getSpatialData().getEntitySpatial();
//                            String blockCadNumber = block.getCadastralNumber();
//                            Geometry boundGeom = makeGeometryByESBound(blockSpatial);
//                            //CadastreEntitySpatial ces = new CadastreEntitySpatial(blockSpatial);
//                            if (boundGeom != null) {
//
//                                Feature q = new Feature(quartalLyr.GetLayerDefn());
//                                q.SetField("cadnumber", blockCadNumber);
//                                q.SetGeometry(boundGeom);
//
//                                quartalLyr.CreateFeature(q);
//                            }
//                            quartalLyr.CommitTransaction();
//                        } catch (Exception ex){
//                            System.err.println(ex.getLocalizedMessage());
//                            quartalLyr.RollbackTransaction();
//                        }

                        // Bounds
                        System.out.println("Processing Bounds...");
//                        try {
//                            if (block.getSubjectBoundaries() != null) {
//                                for (int j = 0; j < block.getSubjectBoundaries().getSubjectBoundaryRecord().size(); j++) {
//
//                                    BoundContoursOut bCntrs = block.getSubjectBoundaries().getSubjectBoundaryRecord().get(j).getBContoursLocation().getContours();
//                                    String descrS = block.getSubjectBoundaries().getSubjectBoundaryRecord().get(j).getBObjectSubjectBoundary().getDescription();
//                                    String RegNumbBorder = block.getSubjectBoundaries().getSubjectBoundaryRecord().get(j).getBObjectSubjectBoundary().getBObject().getRegNumbBorder();
//                                    String TypeBoundary = block.getSubjectBoundaries().getSubjectBoundaryRecord().get(j).getBObjectSubjectBoundary().getBObject().getTypeBoundary().getValue();
//                                    makeGeometryByESBoundList(bCntrs, boundsLyr, descrS, "Субъекты РФ", RegNumbBorder, TypeBoundary);
//
//                                }
//                            }
//                            if (block.getMunicipalBoundaries() != null) {
//                                for (int j = 0; j < block.getMunicipalBoundaries().getMunicipalBoundaryRecord().size(); j++) {
//                                    BoundContoursOut bCntrs = block.getMunicipalBoundaries().getMunicipalBoundaryRecord().get(j).getBContoursLocation().getContours();
//                                    String descrM = block.getMunicipalBoundaries().getMunicipalBoundaryRecord().get(j).getBObjectMunicipalBoundary().getDescription();
//                                    String RegNumbBorder = block.getMunicipalBoundaries().getMunicipalBoundaryRecord().get(j).getBObjectMunicipalBoundary().getBObject().getRegNumbBorder();
//                                    String TypeBoundary = block.getMunicipalBoundaries().getMunicipalBoundaryRecord().get(j).getBObjectMunicipalBoundary().getBObject().getTypeBoundary().getValue();
//                                    makeGeometryByESBoundList(bCntrs, boundsLyr, descrM, "Муниципалитеты", RegNumbBorder, TypeBoundary);
//                                }
//                            }
//                            if (block.getInhabitedLocalityBoundaries() != null) {
//                                for (int j = 0; j < block.getInhabitedLocalityBoundaries().getInhabitedLocalityBoundaryRecord().size(); j++) {
//                                    BoundContoursOut bCntrs = block.getInhabitedLocalityBoundaries().getInhabitedLocalityBoundaryRecord().get(j).getBContoursLocation().getContours();
//                                    String descrI = block.getInhabitedLocalityBoundaries().getInhabitedLocalityBoundaryRecord().get(j).getBObjectInhabitedLocalityBoundary().getDescription();
//                                    String RegNumbBorder = block.getInhabitedLocalityBoundaries().getInhabitedLocalityBoundaryRecord().get(j).getBObjectInhabitedLocalityBoundary().getBObject().getRegNumbBorder();
//                                    String TypeBoundary = block.getInhabitedLocalityBoundaries().getInhabitedLocalityBoundaryRecord().get(j).getBObjectInhabitedLocalityBoundary().getBObject().getTypeBoundary().getValue();
//                                    makeGeometryByESBoundList(bCntrs, boundsLyr, descrI, "Населённые пункты", RegNumbBorder, TypeBoundary);
//                                }
//                            }
//                        } catch (Exception ex) {
//                            System.err.println(ex.getLocalizedMessage());
//                        }
                        System.out.println("Finish Bounds");
                        System.out.println("Processing Zones...");
                        // Zones
                        if (block.getZonesAndTerritoriesBoundaries() != null) {
                            ZonesAndTerritoriesBoundariesType zonesList = block.getZonesAndTerritoriesBoundaries();
                            for (int j = 0; j < zonesList.getZonesAndTerritoriesRecord().size(); j++) {
                                BoundContoursOut bCntrs = zonesList.getZonesAndTerritoriesRecord().get(j).getBContoursLocation().getContours();
                                BobjectZonesAndTerritories record = zonesList.getZonesAndTerritoriesRecord().get(j).getBObjectZonesAndTerritories();
                                String recordName = record.getNameByDoc();
                                String recordDescr = record.getDescription();
                                String recordNumber = record.getNumber();
                                String recordType = record.getTypeZone().getValue();
                                String recordOther = record.getOther();
                                String recordRegNumb = record.getBObject().getRegNumbBorder();
                                String recordTypeBoundary = record.getBObject().getTypeBoundary().getValue();

                                for (int k = 0; k < bCntrs.getContour().size(); k++) {
//                                    Geometry g = makeGeometryByESBound(bCntrs.getContour().get(k).getEntitySpatial());
//                                    if (g != null) {
//                                        try {
//                                            Feature q = new Feature(zonesLyr.GetLayerDefn());
//                                            q.SetField("name", recordName);
//                                            q.SetField("number", recordNumber);
//                                            q.SetField("descr", recordDescr);
//                                            q.SetField("type", recordType);
//                                            q.SetField("regnumber", recordRegNumb);
//                                            q.SetField("typebound", recordName);
//                                            q.SetField("other", recordOther);
//                                            q.SetField("typebound", recordTypeBoundary);
//                                            q.SetGeometry(g);
//
//                                            zonesLyr.CreateFeature(q);
//                                            zonesLyr.CommitTransaction();
//                                        } catch (Exception ex) {
//                                            zonesLyr.RollbackTransaction();
//                                            System.err.println(ex.getMessage());
//                                        }
//                                    }
                                }
                            }
                        }

                        System.out.println("Finish Bounds");

                        System.out.println("Processing realty...");
                        if (block.getRecordData() != null) {
                            // Parcels
                            if (block.getRecordData().getBaseData().getLandRecords() != null) {
                                System.out.println("Processing lands...");
                                BaseDataType.LandRecords land = block.getRecordData().getBaseData().getLandRecords();
                                for (int j = 0; j < land.getLandRecord().size(); j++) {
                                    if (land.getLandRecord().get(j).getContoursLocation() != null) {
                                        for (int k = 0; k < land.getLandRecord().get(j).getContoursLocation().getContours().getContour().size(); k++) {
                                            CadastreEntitySpatial ces = new CadastreEntitySpatial(land.getLandRecord().get(j).getContoursLocation().getContours().getContour().get(k).getEntitySpatial(), false, "","", null, null);
//                                            if (ces.getZUOutGeometry() != null) {
//
//                                                try {
//                                                    Geometry g = ces.getZUOutGeometry();
//                                                    Feature p = new Feature(parcelsLyr.GetLayerDefn());
//                                                    p.SetField("cadnumber", getCadNum(land.getLandRecord().get(j)));
//                                                    p.SetField("comoncadn", getCommonCadNum(land.getLandRecord().get(j)));
//                                                    p.SetField("category", getCategory(land.getLandRecord().get(j)));
//                                                    p.SetField("utildoc", getPermUseByDoc(land.getLandRecord().get(j)));
//                                                    p.SetField("utiltype", getPermUseByType(land.getLandRecord().get(j)));
//                                                    p.SetField("utilgradr", getPermUseByGradReg(land.getLandRecord().get(j)));
//                                                    p.SetField("type", getSubType(land.getLandRecord().get(j)));
//                                                    p.SetField("state", getType(land.getLandRecord().get(j)));
//                                                    p.SetField("area", getArea(land.getLandRecord().get(j)));
//                                                    p.SetField("cadcost", "");
//                                                    p.SetField("location", getLocation(land.getLandRecord().get(j)));
//                                                    p.SetField("adrtype", getAdrType(land.getLandRecord().get(j)));
//                                                    p.SetField("adr1", getAdr1(land.getLandRecord().get(j)));
//                                                    p.SetField("street", getStreet(land.getLandRecord().get(j)));
//                                                    p.SetField("level1", getLeve1(land.getLandRecord().get(j)));
//                                                    p.SetField("level2", getLeve2(land.getLandRecord().get(j)));
//                                                    p.SetField("level3", getLeve3(land.getLandRecord().get(j)));
//                                                    p.SetField("apartment", getAppart(land.getLandRecord().get(j)));
//                                                    p.SetField("other", getOther(land.getLandRecord().get(j)));
//                                                    p.SetField("note", getNote(land.getLandRecord().get(j)));
//
//                                                    p.SetGeometry(g);
//
//                                                    parcelsLyr.CreateFeature(p);
//                                                    parcelsLyr.CommitTransaction();
//
//                                                } catch (Exception ex) {
//                                                    parcelsLyr.RollbackTransaction();
//                                                    System.err.println("Error in create Parcel; " + ex.getMessage());
//                                                }
//                                            }
                                        }
                                    }
                                }
                            }
                            // OKS build
                            if (block.getRecordData().getBaseData().getBuildRecords() != null) {
                                System.out.println("Processing buildings...");
                                BaseDataType.BuildRecords build = block.getRecordData().getBaseData().getBuildRecords();
                                for (int j = 0; j < build.getBuildRecord().size(); j++) {
                                    if (build.getBuildRecord().get(j).getContours() != null) {
                                        for (int k = 0; k < build.getBuildRecord().get(j).getContours().getContour().size(); k++) {
                                            System.out.println("Geometry with " + build.getBuildRecord().get(j).getContours().getContour().size() + " contours. " + getCadNum(build.getBuildRecord().get(j)));
                                            CadastreEntitySpatial ces = new CadastreEntitySpatial(build.getBuildRecord().get(j).getContours().getContour().get(k).getEntitySpatial(), false, "", "");
//                                            if (ces.getOKSOutLineGeometry() != null) {
//                                                List<Geometry> gbl = ces.getOKSOutLineGeometry();
//                                                for (int h=0; h < gbl.size(); h++) {
//                                                    try {
//                                                        oksLineLyr.StartTransaction();
//                                                        Feature b = new Feature(oksLineLyr.GetLayerDefn());
//                                                        b.SetField("cadnumber", getCadNum(build.getBuildRecord().get(j)));
//                                                        b.SetGeometry(gbl.get(h));
//                                                        oksLineLyr.CreateFeature(b);
//                                                        oksLineLyr.CommitTransaction();
//
//                                                    } catch (Exception ex) {
//                                                        oksLineLyr.RollbackTransaction();
//                                                        System.err.println("Error in create Building; " + ex.getMessage());
//                                                    }
//
//                                                }
//                                            }
//                                            if (ces.getOKSOutPolyGeometry() != null) {
//                                                List<Geometry> gbl = ces.getOKSOutPolyGeometry();
//                                                for (int h=0; h < gbl.size(); h++) {
//                                                    try {
//                                                        oksPolyLyr.StartTransaction();
//                                                        Feature b = new Feature(oksPolyLyr.GetLayerDefn());
//                                                        b.SetField("cadnumber", getCadNum(build.getBuildRecord().get(j)));
//                                                        b.SetGeometry(gbl.get(h));
//                                                        oksPolyLyr.CreateFeature(b);
//                                                        oksPolyLyr.CommitTransaction();
//
//                                                    } catch (Exception ex) {
//                                                        oksPolyLyr.RollbackTransaction();
//                                                        System.err.println("Error in create Building; " + ex.getMessage());
//                                                    }
//
//                                                }
//                                            }
                                        }
                                    }
                                }

                            }
                            System.out.println("Processing constructions...");
                            // OKS construction
                            if (block.getRecordData().getBaseData().getConstructionRecords() != null) {
                                BaseDataType.ConstructionRecords contr = block.getRecordData().getBaseData().getConstructionRecords();
                                for (int j = 0; j < contr.getConstructionRecord().size(); j++) {
                                    if (contr.getConstructionRecord().get(j).getContours() != null) {
                                        for (int k = 0; k < contr.getConstructionRecord().get(j).getContours().getContour().size(); k++) {
                                            System.out.println("Geometry with " + contr.getConstructionRecord().get(j).getContours().getContour().size() + " contours " + getCadNum(contr.getConstructionRecord().get(j)));
                                            CadastreEntitySpatial ces = new CadastreEntitySpatial(contr.getConstructionRecord().get(j).getContours().getContour().get(k).getEntitySpatial(), false, "", "");
//                                            if (ces.getOKSOutLineGeometry() != null) {
//                                                List<Geometry> gbl = ces.getOKSOutLineGeometry();
//                                                for (int h=0; h < gbl.size(); h++) {
//                                                    try {
//                                                        oksLineLyr.StartTransaction();
//                                                        Feature b = new Feature(oksLineLyr.GetLayerDefn());
//                                                        b.SetField("cadnumber", getCadNum(contr.getConstructionRecord().get(j)));
//                                                        b.SetGeometry(gbl.get(h));
//                                                        oksLineLyr.CreateFeature(b);
//                                                        oksLineLyr.CommitTransaction();
//
//                                                    } catch (Exception ex) {
//                                                        oksLineLyr.RollbackTransaction();
//                                                        System.err.println("Error in create Building; " + ex.getMessage());
//                                                    }
//
//                                                }
//                                            }
//                                            if (ces.getOKSOutPolyGeometry() != null) {
//                                                List<Geometry> gbl = ces.getOKSOutPolyGeometry();
//                                                for (int h=0; h < gbl.size(); h++) {
//                                                    try {
//                                                        oksPolyLyr.StartTransaction();
//                                                        Feature b = new Feature(oksPolyLyr.GetLayerDefn());
//                                                        b.SetField("cadnumber", getCadNum(contr.getConstructionRecord().get(j)));
//                                                        b.SetGeometry(gbl.get(h));
//                                                        oksPolyLyr.CreateFeature(b);
//                                                        oksPolyLyr.CommitTransaction();
//
//                                                    } catch (Exception ex) {
//                                                        oksPolyLyr.RollbackTransaction();
//                                                        System.err.println("Error in create Building; " + ex.getMessage());
//                                                    }
//
//                                                }
//                                            }
//                                        }
                                        }

                                    }
                                }

                                System.out.println("Processing under constructions...");
                                // OKS under contsruction
                                if (block.getRecordData().getBaseData().getObjectUnderConstructionRecords() != null) {
                                    BaseDataType.ObjectUnderConstructionRecords under = block.getRecordData().getBaseData().getObjectUnderConstructionRecords();
                                    for (int j = 0; j < under.getObjectUnderConstructionRecord().size(); j++) {
                                        if (under.getObjectUnderConstructionRecord().get(j).getContours() != null) {
                                            for (int k = 0; k < under.getObjectUnderConstructionRecord().get(j).getContours().getContour().size(); k++) {
                                                CadastreEntitySpatial ces = new CadastreEntitySpatial(under.getObjectUnderConstructionRecord().get(j).getContours().getContour().get(k).getEntitySpatial(), false, "", "");

                                            }
                                        }

                                    }
                                }
                                System.out.println("Finish realty...");
                            }
                        }

                    }
                }
            }

        } catch (JAXBException ex) {
            System.err.println(ex.getMessage());
        } catch (Exception ex){
            System.err.println(ex.getMessage());
        }

    }


    // Parcels prop
    // Установленное разрешенное использование
    private String getCategory (BaseDataType.LandRecords.LandRecord Land) {
        try {
            if (Land.getParams().getCategory() != null){
                return Land.getParams().getCategory().getType().getValue();
            } else {
                return "";
            }
        } catch (Exception ex) {
            return "";
        }
    }
    // Установленное разрешенное использование
    private String getPermUseByDoc (BaseDataType.LandRecords.LandRecord Land) {
        try {
            if (Land.getParams().getPermittedUse() != null && Land.getParams().getPermittedUse().getPermittedUseEstablished() != null){
                return Land.getParams().getPermittedUse().getPermittedUseEstablished().getByDocument();
            } else {
                return "";
            }
        } catch (Exception ex) {
            return "";
        }
    }
    // Установленное разрешенное использование
    private String getPermUseByType (BaseDataType.LandRecords.LandRecord Land) {
        try {
            if (Land.getParams().getPermittedUse() != null && Land.getParams().getPermittedUse().getPermittedUseEstablished() != null){
                return Land.getParams().getPermittedUse().getPermittedUseEstablished().getLandUse().getValue();
            } else {
                return "";
            }
        } catch (Exception ex) {
            return "";
        }
    }
    // Разрешенное использование по градостроительному регламенту
    private String getPermUseByGradReg (BaseDataType.LandRecords.LandRecord Land) {
        try {
            if (Land.getParams().getPermittesUsesGradReg() != null){
                return Land.getParams().getPermittesUsesGradReg().getPermittedUseText();
            } else {
                return "";
            }
        } catch (Exception ex) {
            return "";
        }
    }
    // Разрешенное использование по градостроительному регламенту
    private double getArea (BaseDataType.LandRecords.LandRecord Land) {
        try {
            if (Land.getParams().getArea() != null){
                return Land.getParams().getArea().getValue().doubleValue();
            } else {
                return -1.0;
            }
        } catch (Exception ex) {
            return -1.0;
        }
    }
    // Общие сведения о земельном участке (вид)
    private String getSubType (BaseDataType.LandRecords.LandRecord Land) {
        try {
            if (Land.getObject().getSubtype() != null){
                return Land.getObject().getSubtype().getValue();
            } else {
                return "";
            }
        } catch (Exception ex) {
            return "";
        }
    }
    // Общие сведения о земельном участке (кадастровый номер)
    private String getCadNum (BaseDataType.LandRecords.LandRecord Land) {
        try {
            if (Land.getObject().getCommonData() != null && Land.getObject().getCommonData().getCadNumber() != null){
                return Land.getObject().getCommonData().getCadNumber();
            } else {
                return "";
            }
        } catch (Exception ex) {
            return "";
        }
    }
    private String getCadNum (BaseDataType.BuildRecords.BuildRecord Build) {
        try {
            if (Build.getObject().getCommonData() != null && Build.getObject().getCommonData().getCadNumber() != null){
                return Build.getObject().getCommonData().getCadNumber();
            } else {
                return "";
            }
        } catch (Exception ex) {
            return "";
        }
    }
    private String getCadNum (BaseDataType.ConstructionRecords.ConstructionRecord Build) {
        try {
            if (Build.getObject().getCommonData() != null && Build.getObject().getCommonData().getCadNumber() != null){
                return Build.getObject().getCommonData().getCadNumber();
            } else {
                return "";
            }
        } catch (Exception ex) {
            return "";
        }
    }
    private String getCommonCadNum (BaseDataType.LandRecords.LandRecord Land) {
        try {
            if (Land.getCadLinks().getCommonLand() != null && (Land.getCadLinks().getCommonLand().getCommonLandCadNumber().getCadNumber() != null)){
                return Land.getCadLinks().getCommonLand().getCommonLandCadNumber().getCadNumber();
            } else {
                return "";
            }
        } catch (Exception ex) {
            return "";
        }
    }
    // Общие сведения о земельном участке (кадастровый номер)
    private String getType (BaseDataType.LandRecords.LandRecord Land) {
        try {
            if (Land.getObject().getCommonData() != null && Land.getObject().getCommonData().getType() != null){
                return Land.getObject().getCommonData().getType().getValue();
            } else {
                return "";
            }
        } catch (Exception ex) {
            return "";
        }
    }
    //
    private String getAdr1 (BaseDataType.LandRecords.LandRecord Land) {
        try {
            if (Land.getAddressLocation().getAddress() == null || Land.getAddressLocation().getAddress().getAddressFias() == null){
                return "";
            }
            String region = Land.getAddressLocation().getAddress().getAddressFias().getLevelSettlement().getRegion().getValue();
            String district = "";
            String city = "";
            String urbanDistrict = "";
            String selsovet = "";
            if (Land.getAddressLocation().getAddress().getAddressFias().getLevelSettlement().getDistrict() != null){
                district = Land.getAddressLocation().getAddress().getAddressFias().getLevelSettlement().getDistrict().getTypeDistrict() +
                      " " +Land.getAddressLocation().getAddress().getAddressFias().getLevelSettlement().getDistrict().getNameDistrict();
            }
            if (Land.getAddressLocation().getAddress().getAddressFias().getLevelSettlement().getCity() != null){
                city = Land.getAddressLocation().getAddress().getAddressFias().getLevelSettlement().getCity().getTypeCity() +
                " "+ Land.getAddressLocation().getAddress().getAddressFias().getLevelSettlement().getCity().getNameCity();
            }
            if (Land.getAddressLocation().getAddress().getAddressFias().getLevelSettlement().getUrbanDistrict() != null){
                urbanDistrict = Land.getAddressLocation().getAddress().getAddressFias().getLevelSettlement().getUrbanDistrict().getTypeUrbanDistrict()+
                " "+Land.getAddressLocation().getAddress().getAddressFias().getLevelSettlement().getUrbanDistrict().getNameUrbanDistrict();
            }
            if (Land.getAddressLocation().getAddress().getAddressFias().getLevelSettlement().getSovietVillage() != null){
                selsovet = Land.getAddressLocation().getAddress().getAddressFias().getLevelSettlement().getSovietVillage().getTypeSovietVillage()+
                " "+Land.getAddressLocation().getAddress().getAddressFias().getLevelSettlement().getSovietVillage().getNameSovietVillage();
            }

            return region + ","+district+","+city+","+urbanDistrict+","+selsovet;
        } catch (Exception ex) {
            return "";
        }
    }
    private String getStreet (BaseDataType.LandRecords.LandRecord Land) {
        try {
            if (Land.getAddressLocation().getAddress().getAddressFias() != null && Land.getAddressLocation().getAddress().getAddressFias().getDetailedLevel().getStreet() != null){
                return  Land.getAddressLocation().getAddress().getAddressFias().getDetailedLevel().getStreet().getTypeStreet()+""+
                      Land.getAddressLocation().getAddress().getAddressFias().getDetailedLevel().getStreet().getNameStreet();
            } else {
                return "";
            }
        } catch (Exception ex) {
            return "";
        }
    }
    private String getLeve1 (BaseDataType.LandRecords.LandRecord Land) {
        try {
            if (Land.getAddressLocation().getAddress().getAddressFias() != null && Land.getAddressLocation().getAddress().getAddressFias().getDetailedLevel().getLevel1() != null){
                return  Land.getAddressLocation().getAddress().getAddressFias().getDetailedLevel().getLevel1().getTypeLevel1()+" "+
                      Land.getAddressLocation().getAddress().getAddressFias().getDetailedLevel().getLevel1().getNameLevel1();
            } else {
                return "";
            }
        } catch (Exception ex) {
            return "";
        }
    }
    private String getLeve2 (BaseDataType.LandRecords.LandRecord Land) {
        try {
            if (Land.getAddressLocation().getAddress().getAddressFias() != null && Land.getAddressLocation().getAddress().getAddressFias().getDetailedLevel().getLevel2() != null){
                return  Land.getAddressLocation().getAddress().getAddressFias().getDetailedLevel().getLevel2().getTypeLevel2()+" "+
                      Land.getAddressLocation().getAddress().getAddressFias().getDetailedLevel().getLevel2().getNameLevel2();
            } else {
                return "";
            }
        } catch (Exception ex) {
            return "";
        }
    }
    private String getLeve3 (BaseDataType.LandRecords.LandRecord Land) {
        try {
            if (Land.getAddressLocation().getAddress().getAddressFias() != null && Land.getAddressLocation().getAddress().getAddressFias().getDetailedLevel().getLevel3() != null){
                return  Land.getAddressLocation().getAddress().getAddressFias().getDetailedLevel().getLevel3().getTypeLevel3()+" "+
                      Land.getAddressLocation().getAddress().getAddressFias().getDetailedLevel().getLevel3().getNameLevel3();
            } else {
                return "";
            }
        } catch (Exception ex) {
            return "";
        }
    }
    private String getAppart (BaseDataType.LandRecords.LandRecord Land) {
        try {
            if (Land.getAddressLocation().getAddress().getAddressFias() != null && Land.getAddressLocation().getAddress().getAddressFias().getDetailedLevel().getApartment() != null){
                return  Land.getAddressLocation().getAddress().getAddressFias().getDetailedLevel().getApartment().getTypeApartment()+" "+
                      Land.getAddressLocation().getAddress().getAddressFias().getDetailedLevel().getApartment().getNameApartment();
            } else {
                return "";
            }
        } catch (Exception ex) {
            return "";
        }
    }
    private String getAdrType (BaseDataType.LandRecords.LandRecord Land) {
        try {
            if (Land.getAddressLocation().getAddressType() != null){
                return  Land.getAddressLocation().getAddressType().getValue();
            } else {
                return "";
            }
        } catch (Exception ex) {
            return "";
        }
    }
    private String getOther (BaseDataType.LandRecords.LandRecord Land) {
        try {
            if (Land.getAddressLocation().getAddress().getAddressFias() != null && Land.getAddressLocation().getAddress().getAddressFias().getDetailedLevel().getOther() != null){
                return  Land.getAddressLocation().getAddress().getAddressFias().getDetailedLevel().getOther();
            } else {
                return "";
            }
        } catch (Exception ex) {
            return "";
        }
    }
    private String getNote (BaseDataType.LandRecords.LandRecord Land) {
        try {
            if (Land.getAddressLocation() != null && Land.getAddressLocation().getAddress().getNote() != null){
                return  Land.getAddressLocation().getAddress().getNote();
            } else {
                return "";
            }
        } catch (Exception ex) {
            return "";
        }
    }
    private String getLocation (BaseDataType.LandRecords.LandRecord Land) {
        try {
            String loc = "";

            if (Land.getAddressLocation() != null && Land.getAddressLocation().getRelPosition().getLocationDescription() != null){
                loc +=  Land.getAddressLocation().getRelPosition().getLocationDescription();
            }
            if (Land.getAddressLocation() != null && Land.getAddressLocation().getRelPosition().getRefPointName() != null){
                loc += " " + Land.getAddressLocation().getRelPosition().getRefPointName();
            }
            return loc;
        } catch (Exception ex) {
            return "";
        }
    }

    // helpers


}
